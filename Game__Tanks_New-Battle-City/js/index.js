/* Main goal of the game is to gain points and set new highscore 
that will be stored in LocalStorage. When you press start, AJAX-request sends 
to get initial field HTML, because field is destroying during the game 
Changing difficulty level changes speed of shooting and moving of enemies
There is also a mobile version of the game, but JS is working worse on phones
Game uploaded to the server - https://artpvideo.000webhostapp.com/itea/TEST_Tanks/index.html*/

const $ = tag => document.querySelector(tag), // shortening selecting of element
$All = tag => document.querySelectorAll(tag), // elements

field = {
  scoreText: $('.score'), // score html text field
  score: 0, // score counter
  time: $('.time'), // time html text field
  timer: 0, // seconds counter
  hearts: $All('.heart'), // hearts html field
  lives: 3, // score counter
  timePlayed: window.localStorage.timePlayed ? parseInt(window.localStorage.timePlayed) : 0, // game played counter
  difficulty: $('#difficulty').value, // value of select element

  startGame() { // game starting function
    this.timer = 0; // setting initial values
    this.score = 0;
    this.lives = 3;
    this.difficulty = $('#difficulty').value; // setting difficulty level
    game.started = true; // variable that will be used after, indicates if game is started

    if($('#name').value.length < 2 || $('#name').value.length > 15) { // name validator
      $('#name').classList = 'wrong';
      $('#error').classList = '';
    } else {
      $('#name').classList = 'hide'; // if length is correct, hides starting info
      $('#namelabel').classList = 'hide';
      $('#error').classList = 'hide';
      $('h2').innerHTML = $('#name').value;
      if($('.yourscore').classList !== 'yourscore hide') {
        $All('.yourscore').forEach(each => each.classList = 'yourscore hide');
      }
      
      this.showHearts() // updates hearts
      this.createEnemies(); // creates enemies
      this.runTime(); // starts timer
      this.scoreText.innerHTML = this.score; // sets score 0
      setTimeout(() => game.startShooting(), 2000); // enemies start shooting 

      $('#main').play(); // playing sound // music settings
      setTimeout(() => $('#music').play(), 4500);
      $('#music').volume = 0.05;

      $('.black_part1').classList = 'black black_part1-start'; // hiding and showing page elements
      $('.black_part2').classList = 'black black_part2-start';
      $('#firstStart').classList = 'hide';
      $('#difficulty').classList = 'hide';   
      $('h1').classList = 'hide';
      $('.now').classList = 'now';
      $('.flag').setAttribute('src', './images/flag.png');     
      $All('img.hide').forEach(each => each.parentNode.removeChild(each)); // removes hidden bullets and explosions
    }
  },

  showHearts() {
    for (i = 0; i < 3; i++) {
      this.hearts[i].classList = 'heart';
    }
  },

  runTime(stop) { // starts and stops timer
    if(!stop) { 
      window.timerInt = setInterval(() => {
      this.timer++;
      this.time.innerHTML = field.timer;   
      }, 1000);    
    } else window.clearInterval(timerInt);
  },

  setScore() { // setting current score
    this.score += (30 - this.difficulty / 10);
    this.scoreText.innerHTML = this.score;
  },

  setLives() { // setting current lives
    if(this.lives > 1) {
      this.hearts[this.lives - 1].classList = 'heart hide';
      this.lives -= 1;
    } else {
      this.hearts[this.lives - 1].classList = 'heart hide';
      this.lives -= 1;
      this.gameOver();
    }
  },

  createField() { // sends AJAX-request and gets field HTML
    const fieldAjax = new XMLHttpRequest();  
    fieldAjax.open('GET', './ajax/field.html');
    fieldAjax.addEventListener('load', function() {
      if (fieldAjax.status == 200) {
        $('.field').innerHTML = fieldAjax.responseText;
        field.startGame();
      }
    });
    fieldAjax.send();
  },

  createEnemies() { // creates enemies    
    const rowArr = Array.from(me.parentNode.parentNode.childNodes),  // array with cells
    allFieldArr = Array.from(me.parentNode.parentNode.parentNode.childNodes); // array with colomns  
    
    for (ne = 5; ne <= 95; ne += 30) { // creates in column 5, 35, 65, 95
      const newEnemy = document.createElement('img');
      newEnemy.setAttribute('alt', 'enemy');
      newEnemy.setAttribute('src', './images/flash.gif'); // first flash animation
      newEnemy.classList = 'enemy down';
      allFieldArr[ne].childNodes[5].append(newEnemy);
      setTimeout(() => { 
        newEnemy.setAttribute('src', './images/enemy.png'); // flash changes to tank
        const enemiesArr = Array.from($All('.enemy')),
        enInd = enemiesArr.indexOf(newEnemy);
        game.enemiesMoving(enInd);
      }, 1500);
    }
  },

  gameOver() { // game over fucntion
    game.started = false; // changes game variable
    this.timePlayed += 1; // increases counter times played

    window.localStorage.setItem('timePlayed', this.timePlayed); // adding counters to LocalStorage
    window.localStorage.setItem(`Name_${this.timePlayed}`, $('h2').innerText);
    window.localStorage.setItem(`Score_${this.timePlayed}`, this.score);
    window.localStorage.setItem(`Time_${this.timePlayed}`, this.timer);

    this.runTime(true); // stops timer
    this.setHighscores(); // filling highscores table

    const enemies = $All('.enemy'); // selecting enemies and removing
    enemies.forEach(enemy => enemy.remove());
    $('#me').classList = 'hide'; // hiding my tank

    $('#losemusic').play(); // playing sound // changing music
    $('#main').pause();
    $('#music').pause();

    setTimeout(() => { // after 2s+1s initial starting page will be shown
      $('.black_part1-start').classList = 'black black_part1';
      $('.black_part2-start').classList = 'black black_part2';
      setTimeout(() => { 
        $('#firstStart').classList = '';
        $('#difficulty').classList = '';  
        $('h1').classList = '';
        $('.now').classList = 'now hide';
        $All('.yourscore').forEach(each => each.classList = 'yourscore');
        $All('.yourscore')[1].innerHTML = this.score;
      }, 1000);
    }, 2000);
  },

  flagTaken() { // when shooting at flag
    $('.flag').setAttribute('src', './images/ash.png');
    $('#lose').play(); // playing sound
    
    this.gameOver();
  },

  setHighscores() { // setting highscores from LocalStorage
    const highscoresArr = []; // array with arrays
    for (n = 1; n <= this.timePlayed; n++) {
      const oneGame = []; // array with data
      oneGame.push(window.localStorage.getItem(`Name_${n}`));
      oneGame.push(window.localStorage.getItem(`Score_${n}`));
      oneGame.push(window.localStorage.getItem(`Time_${n}`));
      highscoresArr.push(oneGame);
      highscoresArr.sort((a, b) => b[1] - a[1]); // sorting highscores
      if(n === this.timePlayed) { // if data is collected  
        $('.lastscore').innerText = window.localStorage.getItem(`Score_${this.timePlayed}`); // adding last score info
        const minutes = highscoresArr.reduce((sum, current) => sum + parseInt(current[2]) / 60, 0), // adding total time played
        seconds = (minutes - Math.floor(minutes)) * 60,
        secondsFixed = seconds.toFixed().toString();
        $('.totaltime').innerText = `${Math.floor(minutes)}:${secondsFixed.padStart(2, "0")}`; // adding total time played
        for (j = 1; j <= 10; j++) { // showing highscores on page
          if(j > this.timePlayed) break;
          $(`.place${j}`).innerText = highscoresArr[j - 1][0];
          $(`.score${j}`).innerText = highscoresArr[j - 1][1];
          $(`.time${j}`).innerText = highscoresArr[j - 1][2];
        }
      }
    }
  }
},

// GAME OBJECT
game = {
  shotTimeEnded: true, // if shot is finished 
  started: false, // if game is started
  
  moveLeft() { // moving left function for my tank
    const me = $('#me'), // my tank
    rowArr = Array.from(me.parentNode.parentNode.childNodes), // array with cells  
    current = rowArr.indexOf(me.parentNode), // cell index
    allFieldArr = Array.from(me.parentNode.parentNode.parentNode.childNodes), // field array with colomns
    currentRow = allFieldArr.indexOf(me.parentNode.parentNode), // colomn index
    previousRow = allFieldArr[currentRow - 4], // previous row
    near = previousRow.childNodes[current], // cell near in previous colomn
    up = previousRow.childNodes[current - 2], // cell above in previous colomn
    down = previousRow.childNodes[current + 2]; // cell below in previous colomn

    me.classList = 'left'; // rotates tank

    if(allFieldArr[currentRow - 4].childNodes[current].classList.value === 'box' && // checks for obstructions
    allFieldArr[currentRow - 4].childNodes[current + 2].classList.value === 'box' &&
    allFieldArr[currentRow - 4].childNodes[current - 2].classList.value === 'box') { 
      if(near.childNodes.length > 0 ? near.childNodes[near.childNodes.length - 1].alt !== 'enemy' : game.started && // checks for other tanks              
      up.childNodes.length > 0 ? up.childNodes[up.childNodes.length - 1].alt !== 'enemy' : game.started &&
      down.childNodes.length > 0 ? down.childNodes[down.childNodes.length - 1].alt !== 'enemy' : game.started) {                  
        allFieldArr[currentRow - 2].childNodes[current].append(me); // moves tank
        if(game.controls.once) { // setting timeout 0.5s before acceletation
          game.controls.flag = false;
          game.controls.moveTimeout = setTimeout(() => game.controls.flag = true, 500);
          game.controls.once = false;
        }
      }
    }
  },

  moveRight() { // moving right function for my tank
    const me = $('#me'), // my tank
    rowArr = Array.from(me.parentNode.parentNode.childNodes), // array with cells  
    current = rowArr.indexOf(me.parentNode), // cell index
    allFieldArr = Array.from(me.parentNode.parentNode.parentNode.childNodes), // field array with colomns
    currentRow = allFieldArr.indexOf(me.parentNode.parentNode), // colomn index
    nextRow = allFieldArr[currentRow + 4], // next row
    near = nextRow.childNodes[current], // cell near in next colomn
    up = nextRow.childNodes[current - 2], // cell above in next colomn
    down = nextRow.childNodes[current + 2]; // cell below in next colomn
    
    me.classList = 'right'; // rotates tank

    if(allFieldArr[currentRow + 4].childNodes[current].classList.value === 'box' && // checks for obstructions
    allFieldArr[currentRow + 4].childNodes[current + 2].classList.value === 'box' &&
    allFieldArr[currentRow + 4].childNodes[current - 2].classList.value === 'box') {
      if(near.childNodes.length > 0 ? near.childNodes[near.childNodes.length - 1].alt !== 'enemy' : game.started && // checks for other tanks
      up.childNodes.length > 0 ? up.childNodes[up.childNodes.length - 1].alt !== 'enemy' : game.started &&
      down.childNodes.length > 0 ? down.childNodes[down.childNodes.length - 1].alt !== 'enemy' : game.started) {
        allFieldArr[currentRow + 2].childNodes[current].append(me); // moves tank
        if(game.controls.once) { // setting timeout 0.5s before acceletation
          game.controls.flag = false;
          game.controls.moveTimeout = setTimeout(() => game.controls.flag = true, 500);
          game.controls.once = false;
        }
      }
    }
  },

  moveUp() { // moving up function for my tank
    const me = $('#me'), // my tank
    rowArr = Array.from(me.parentNode.parentNode.childNodes), // array with cells 
    current = rowArr.indexOf(me.parentNode), // cell index
    allFieldArr = Array.from(me.parentNode.parentNode.parentNode.childNodes), // field array with colomns 
    currentRow = allFieldArr.indexOf(me.parentNode.parentNode), // colomn index
    near = rowArr[current - 4], // cell above
    leftBox = allFieldArr[currentRow - 2].childNodes[current - 4], // cell above in previous column
    rightBox = allFieldArr[currentRow + 2].childNodes[current - 4] // cell above in next column

    me.classList = ''; // rotates tank

    if(rowArr[current - 4].classList.value === 'box' && // checks for obstructions
    allFieldArr[currentRow - 2].childNodes[current - 4].classList.value === 'box' &&
    allFieldArr[currentRow + 2].childNodes[current - 4].classList.value === 'box') {
      if(near.childNodes.length > 0 ? near.childNodes[near.childNodes.length - 1].alt !== 'enemy' : game.started && // checks for other tanks
      leftBox.childNodes.length > 0 ? leftBox.childNodes[leftBox.childNodes.length - 1].alt !== 'enemy' : game.started &&
      rightBox.childNodes.length > 0 ? rightBox.childNodes[rightBox.childNodes.length - 1].alt !== 'enemy' : game.started) {
        rowArr[current - 2].append(me) // moves tank
        if(game.controls.once) { // setting timeout 0.5s before acceletation
          game.controls.flag = false;
          game.controls.moveTimeout = setTimeout(() => game.controls.flag = true, 500); 
          game.controls.once = false;
        }
      }
    }
  },

  moveDown() { // moving down function for my tank
    const me = $('#me'), // my tank
    rowArr = Array.from(me.parentNode.parentNode.childNodes), // array with cells 
    current = rowArr.indexOf(me.parentNode), // cell index
    allFieldArr = Array.from(me.parentNode.parentNode.parentNode.childNodes), // field array with colomns 
    currentRow = allFieldArr.indexOf(me.parentNode.parentNode), // colomn index
    near = rowArr[current + 4],
    leftBox = allFieldArr[currentRow - 2].childNodes[current + 4], // cell above in previous column
    rightBox = allFieldArr[currentRow + 2].childNodes[current + 4]; // cell above in next column

    me.classList = 'down'; // rotates tank

    if(rowArr[current + 4].classList.value === 'box' && // checks for obstructions
    allFieldArr[currentRow - 2].childNodes[current + 4].classList.value === 'box' &&
    allFieldArr[currentRow + 2].childNodes[current + 4].classList.value === 'box') {
      if(near.childNodes.length > 0 ? near.childNodes[near.childNodes.length - 1].alt !== 'enemy' : game.started && // checks for other tanks           
      leftBox.childNodes.length > 0 ? leftBox.childNodes[leftBox.childNodes.length - 1].alt !== 'enemy' : game.started &&
      rightBox.childNodes.length > 0 ? rightBox.childNodes[rightBox.childNodes.length - 1].alt !== 'enemy' : game.started) {
        rowArr[current + 2].append(me) // moves tank
        if(game.controls.once) { // setting timeout 0.5s before acceletation
          game.controls.flag = false;
          game.controls.moveTimeout = setTimeout(() => game.controls.flag = true, 500);
          game.controls.once = false;
        }
      }
    }
  },

  controls: { // key controls
    flag: true, // first indicator for timeout 0.5s before acceletation
    once: true, //  second indicator for timeout 0.5s before acceletation
    moveTimeout: undefined, // property to store timeout for moving

    keydown: window.addEventListener('keydown', event => { // keydown events
      if(game.started) {
        if(game.controls.flag && event.keyCode === 37) game.moveLeft(); // moving events
        if(game.controls.flag && event.keyCode === 38) game.moveUp();
        if(game.controls.flag && event.keyCode === 39) game.moveRight();
        if(game.controls.flag && event.keyCode === 40) game.moveDown();
        if(event.keyCode === 32 && game.shotTimeEnded) { // shot event
          game.shotTimeEnded = false;
          setTimeout(() => game.shotTimeEnded = true, 800);

          $('#shot').play(); // playing sound
          game.iShot();
        }
      }
    }),

    keyup: window.addEventListener('keyup', event => { // keyup sets indicators for timeout to initial
      game.controls.once = true;
      game.controls.flag = true;
      clearTimeout(game.controls.moveTimeout); // clears timeout before acceletation
    })
  },
  
  iShot() { // function for player's shooting
    const me = $('#me'),
    rowArr = Array.from(me.parentNode.parentNode.childNodes), // array with cells 
    current = rowArr.indexOf(me.parentNode), // cell index
    allFieldArr = Array.from(me.parentNode.parentNode.parentNode.childNodes),// field array with colomns 
    currentRow = allFieldArr.indexOf(me.parentNode.parentNode); // colomn index
    
    const bullet = document.createElement('img'); // creates bullet
    bullet.setAttribute('alt', 'bullet');
    bullet.setAttribute('src', './images/bullet.png');
    bullet.classList = 'bullet';

    const explosion = document.createElement('img'); // creates explosion
    explosion.setAttribute('alt', 'explosion');
    explosion.setAttribute('src', './images/explosion.gif');
    explosion.classList = 'explosion';

    const gunfire = document.createElement('img'); // creates fire
    gunfire.setAttribute('alt', 'gunfire');
    gunfire.setAttribute('src', './images/gunfire.gif');
    gunfire.classList = 'gunfire';

    if(me.classList == 'right') { // check if tank is rotated
      bullet.classList = 'bullet right'; // rotates bullet
      gunfire.classList = 'gunfire right'; // rotates fire 
      function shot(num) { 
        const nextRow = allFieldArr[currentRow + num + 2], // next row after bullet
        near = nextRow.childNodes[current], // cell near bullet in next row
        up = nextRow.childNodes[current - 2], // next row after bullet, cell above
        down = nextRow.childNodes[current + 2], // next row after bullet, cell below
        upup = nextRow.childNodes[current - 4]; // next row after bullet, 2 cells above
        
        if(near.classList.value !== 'box water' && // checks if no obtruction
          up.classList.value !== 'box water' &&
          down.classList.value !== 'box water') {
            if(near.classList.value !== 'box' ||
              up.classList.value !== 'box' ||
              down.classList.value !== 'box') {
              $('#obstruction').play(); // playing sound // if there is an obstruction shows explosion and hides bullet
              clearInterval(shooting);
              allFieldArr[currentRow + num].childNodes[current].append(explosion);
              bullet.classList = 'hide';
              setTimeout(() => explosion.classList = 'hide', 600);
              
              // SHOT IN WALL
              if(near.classList.value === 'box ground' || // check if there is a wall after bullet              
                up.classList.value === 'box ground' ||
                down.classList.value === 'box ground') {
                near.classList.value = 'box'; // destroys the wall   
                up.classList.value = 'box';
                down.classList.value = 'box';
              }
            }
        }  
        // SHOT IN ENEMY
        // checks if img has "enemy" alt value
        if(near.childNodes.length > 0 ? near.childNodes[near.childNodes.length - 1].alt === 'enemy' : console.log() ||               
        up.childNodes.length > 0 ? up.childNodes[up.childNodes.length - 1].alt === 'enemy' : console.log() ||
        down.childNodes.length > 0 ? down.childNodes[down.childNodes.length - 1].alt === 'enemy' : console.log()) {
          $('#enemyKill').play(); // playing sound
          $('#enemyShot').play(); // playing sound

          // clears interval, shows explosion, hides bullet
          clearInterval(shooting);
          allFieldArr[currentRow + num].childNodes[current].append(explosion);
          bullet.classList = 'hide';
          setTimeout(() => explosion.classList = 'hide', 600);

          // recreates enemy
          const newEnemy = document.createElement('img');
          newEnemy.setAttribute('src', './images/flash.gif');
          newEnemy.classList = 'enemy down';
          allFieldArr[95].childNodes[5].append(newEnemy);
          setTimeout(() => { 
            newEnemy.setAttribute('src', './images/enemy.png');
            newEnemy.setAttribute('alt', 'enemy');
            const enemiesArr = Array.from($All('.enemy')),
            enInd = enemiesArr.indexOf(newEnemy);
            game.enemiesMoving(enInd);
          }, 1500);

          // removes killed enemy
          near.innerHTML = '';             
          up.innerHTML = '';
          down.innerHTML = '';
          
          field.setScore(); // set new score
        }
        // SHOT IN FLAG
        if(near.childNodes.length > 0 ? near.childNodes[0].alt === 'flag' : console.log() || // checks for alt value          
        up.childNodes.length > 0 ? up.childNodes[0].alt === 'flag' : console.log() ||
        down.childNodes.length > 0 ? down.childNodes[0].alt === 'flag' : console.log() ||
        upup.childNodes.length > 0 ? upup.childNodes[0].alt === 'flag' : console.log()) {
          field.flagTaken(); // game over
        };
        allFieldArr[currentRow + num].childNodes[current].append(gunfire); // shows fire
        setTimeout(() => gunfire.classList = 'hide', 200); // hides fire
        allFieldArr[currentRow + num].childNodes[current].append(bullet); // moves bullet on field
      }
      let num = 0; // initial value
      const shooting = setInterval(() => shot(num += 2), 40); // continues shooting
    }

    if(me.classList == 'left') { // check if tank is rotated
      bullet.classList = 'bullet left' // rotates bullet
      gunfire.classList = 'gunfire left' // rotates fire 
      function shot(num) { 
        const previousRow = allFieldArr[currentRow + num - 2], // column before bullet
        near = previousRow.childNodes[current], // colomn before bullet
        up = previousRow.childNodes[current - 2], // column before bullet, cell above
        down = previousRow.childNodes[current + 2] // column before bullet, cell below
        
        if(near.classList.value !== 'box water' && // checks if there is an obstruction
          up.classList.value !== 'box water' &&
          down.classList.value !== 'box water') {
            if(near.classList.value !== 'box' ||
              up.classList.value !== 'box' ||
              down.classList.value !== 'box') {
              $('#obstruction').play(); // playing sound

              // clears interval, shows explosion, hides bullet
              clearInterval(shooting);
              allFieldArr[currentRow + num].childNodes[current].append(explosion);
              bullet.classList = 'hide';
              setTimeout(() => explosion.classList = 'hide', 600);
              
              // SHOT IN WALL
              if(near.classList.value === 'box ground' || // check if there is a wall after bullet             
                up.classList.value === 'box ground' ||
                down.classList.value === 'box ground') {
                near.classList.value = 'box';     
                up.classList.value = 'box';
                down.classList.value = 'box';
              }
            }
        }
        // SHOT IN ENEMY
        // checks if img has "enemy" alt value
        if(near.childNodes.length > 0 ? near.childNodes[near.childNodes.length - 1].alt === 'enemy' : console.log() ||               
          up.childNodes.length > 0 ? up.childNodes[up.childNodes.length - 1].alt === 'enemy' : console.log() ||
          down.childNodes.length > 0 ? down.childNodes[down.childNodes.length - 1].alt === 'enemy' : console.log()) {
          $('#enemyKill').play(); // playing sound
          $('#enemyShot').play(); // playing sound
            
          // clears interval, shows explosion, hides bullet
          clearInterval(shooting);
          allFieldArr[currentRow + num].childNodes[current].append(explosion);
          bullet.classList = 'hide';
          setTimeout(() => explosion.classList = 'hide', 600);
          
          // recreates enemy
          const newEnemy = document.createElement('img');
          newEnemy.setAttribute('src', './images/flash.gif');
          newEnemy.classList = 'enemy down';
          allFieldArr[65].childNodes[5].append(newEnemy);
          setTimeout(() => { 
            newEnemy.setAttribute('src', './images/enemy.png');
            newEnemy.setAttribute('alt', 'enemy');
            const enemiesArr = Array.from($All('.enemy')),
            enInd = enemiesArr.indexOf(newEnemy);
            game.enemiesMoving(enInd);
          }, 1500)

          // removes killed enemy
          near.innerHTML = '';
          up.innerHTML = '';
          down.innerHTML = '';

          field.setScore(); // sets new score
        } 
        // SHOT IN FLAG
        if(near.childNodes.length > 0 ? near.childNodes[0].alt === 'flag' : console.log() || // checks for "flag" alt value              
          up.childNodes.length > 0 ? up.childNodes[0].alt === 'flag' : console.log() ||
          down.childNodes.length > 0 ? down.childNodes[0].alt === 'flag' : console.log()) {
          field.flagTaken(); // game over
        }
        if(currentRow + num - 6 > 0) { // for shot from the right
          if(allFieldArr[currentRow + num - 6].childNodes[current - 4].childNodes.length > 0 ? 
            allFieldArr[currentRow + num - 6].childNodes[current - 4].childNodes[0].alt === 'flag' : console.log()) {
            field.flagTaken(); // game over
          }
        }
        // shows fire and moves bullet
        allFieldArr[currentRow + num].childNodes[current].append(gunfire);
        setTimeout(() => gunfire.classList = 'hide', 200);
        allFieldArr[currentRow + num].childNodes[current].append(bullet);
      }
      let num = 0; // initial value
      const shooting = setInterval(() => shot(num -= 2), 40); // continues shooting
    }

    if(me.classList == '') { // if heading up
      bullet.classList = 'bullet'; // rotates bullet and fire
      gunfire.classList = 'gunfire';
      function shot(num) { 
        const near = rowArr[current + num - 2],
        leftBox = allFieldArr[currentRow - 2].childNodes[current + num - 2],
        rightBox = allFieldArr[currentRow + 2].childNodes[current + num - 2];
        
        if(near.classList.value !== 'box water' && // checks if there is an obstruction
          leftBox.classList.value !== 'box water' &&
          rightBox.classList.value !== 'box water') {
            if(near.classList.value !== 'box' ||
              leftBox.classList.value !== 'box' ||
              rightBox.classList.value !== 'box') {
              $('#obstruction').play(); // playing sound

              // clears interval, shows explosion, hides bullet
              clearInterval(shooting);
              rowArr[current + num].append(explosion);
              bullet.classList = 'hide';
              setTimeout(() => explosion.classList = 'hide', 600);
              
              // SHOT IN WALL
              if(near.classList.value === 'box ground' || // check if there is a wall after bullet
                leftBox.classList.value === 'box ground' ||
                rightBox.classList.value === 'box ground') {
                near.classList.value = 'box'; // destroys wall           
                leftBox.classList.value = 'box';
                rightBox.classList.value = 'box';
              }
            }
        }

        // SHOT IN ENEMY
        // checks if img has "enemy" alt value
        if(near.childNodes.length > 0 ? near.childNodes[near.childNodes.length - 1].alt === 'enemy' : console.log() ||               
          leftBox.childNodes.length > 0 ? leftBox.childNodes[leftBox.childNodes.length - 1].alt === 'enemy' : console.log() ||
          rightBox.childNodes.length > 0 ? rightBox.childNodes[rightBox.childNodes.length - 1].alt === 'enemy' : console.log()) {
          $('#enemyKill').play(); // playing sound
          $('#enemyShot').play(); // playing sound

          // clears interval, shows explosion, hides bullet
          clearInterval(shooting);
          rowArr[current + num].append(explosion);
          bullet.classList = 'hide';
          setTimeout(() => explosion.classList = 'hide', 600);

          // recreates enemy
          const newEnemy = document.createElement('img');
          newEnemy.setAttribute('src', './images/flash.gif');
          newEnemy.classList = 'enemy down';
          allFieldArr[35].childNodes[5].append(newEnemy);
          setTimeout(() => { 
            newEnemy.setAttribute('src', './images/enemy.png');
            newEnemy.setAttribute('alt', 'enemy');
            const enemiesArr = Array.from($All('.enemy')),
            enInd = enemiesArr.indexOf(newEnemy);
            game.enemiesMoving(enInd);
          }, 1500);

          // removes killed enemy
          near.innerHTML = '';        
          leftBox.innerHTML = '';
          rightBox.innerHTML = '';

          field.setScore() // sets new score
        }
        // shows fire and moves bullet
        rowArr[current + num].append(gunfire);
        setTimeout(() => gunfire.classList = 'hide', 200);
        rowArr[current + num].append(bullet);
      }
      let num = 0; // initial value
      const shooting = setInterval(() => shot(num -= 2), 40); // continues shooting
    }

    if(me.classList == 'down') { // if heading down
      bullet.classList = 'bullet down'; // rotates bullet and gunfire
      gunfire.classList = 'gunfire down';
      function shot(num) { 
        near = rowArr[current + num + 2], // cell after bullet
        leftBox = allFieldArr[currentRow - 2].childNodes[current + num + 2], // cell after bullet and to the left
        rightBox = allFieldArr[currentRow + 2].childNodes[current + num + 2], // cell after bullet and to the right
        leftleftBox = allFieldArr[currentRow - 4].childNodes[current + num + 2]; // cell after bullet and 2 cells to the left
        
        if(near.classList.value !== 'box water' && // checks if there is an obstruction
          leftBox.classList.value !== 'box water' &&
          rightBox.classList.value !== 'box water') {
            if(near.classList.value !== 'box' ||
              leftBox.classList.value !== 'box' ||
              rightBox.classList.value !== 'box') {
              $('#obstruction').play(); // playing sound

              // clears interval, shows explosion, hides bullet
              clearInterval(shooting);
              rowArr[current + num].append(explosion);
              bullet.classList = 'hide';
              setTimeout(() => explosion.classList = 'hide', 600);
              
              // SHOT IN WALL
              if(near.classList.value === 'box ground' || // check if there is a wall after bullet             
                leftBox.classList.value === 'box ground' ||
                rightBox.classList.value === 'box ground') {
                near.classList.value = 'box'; // destroys wall           
                leftBox.classList.value = 'box';
                rightBox.classList.value = 'box';
              }
            }
        }
        // SHOT IN ENEMY
        // checks if img has "enemy" alt value
        if(near.childNodes.length > 0 ? near.childNodes[near.childNodes.length - 1].alt === 'enemy' : console.log() ||               
          leftBox.childNodes.length > 0 ? leftBox.childNodes[leftBox.childNodes.length - 1].alt === 'enemy' : console.log() ||
          rightBox.childNodes.length > 0 ? rightBox.childNodes[rightBox.childNodes.length - 1].alt === 'enemy' : console.log()) {
          $('#enemyKill').play(); // playing sound
          $('#enemyShot').play(); // playing sound

          // clears interval, shows explosion, hides bullet
          clearInterval(shooting);
          rowArr[current + num].append(explosion);
          bullet.classList = 'hide';
          setTimeout(() => explosion.classList = 'hide', 600);

          //creates new enemy
          const newEnemy = document.createElement('img');
          newEnemy.setAttribute('src', './images/flash.gif');
          newEnemy.classList = 'enemy down';
          allFieldArr[5].childNodes[5].append(newEnemy);
          setTimeout(() => { 
            newEnemy.setAttribute('src', './images/enemy.png');
            newEnemy.setAttribute('alt', 'enemy');
            const enemiesArr = Array.from($All('.enemy')),
            enInd = enemiesArr.indexOf(newEnemy);
            game.enemiesMoving(enInd);
          }, 1500);

          // removes killed enemy
          near.innerHTML = '';
          leftBox.innerHTML = '';
          rightBox.innerHTML = '';

          field.setScore(); // updates score
        }
        // SHOT IN FLAG
        if(near.childNodes.length > 0 ? near.childNodes[0].alt === 'flag' : console.log() || // checks for flag alt value              
          leftBox.childNodes.length > 0 ? leftBox.childNodes[0].alt === 'flag' : console.log() ||
          rightBox.childNodes.length > 0 ? rightBox.childNodes[0].alt === 'flag' : console.log() ||
          leftleftBox.childNodes.length > 0 ? leftleftBox.childNodes[0].alt === 'flag' : console.log()) {
          field.flagTaken(); // game over
        }
        // moves bullet and showns fire at beggining
        rowArr[current + num].append(gunfire);
        setTimeout(() => gunfire.classList = 'hide', 200);
        rowArr[current + num].append(bullet);
      }
      let num = 0; // initial value
      const shooting = setInterval(() => shot(num += 2), 40); // continues shooting
    }
  },

  enemiesMoving(newEn) { // function for moving of enemies
    const enemies = $All('.enemy'); // enemies array

    function moveUp(en) { 
      const rowArr = Array.from(enemies[en].parentNode.parentNode.childNodes),  // colomn array with cells    
      current = rowArr.indexOf(enemies[en].parentNode), // index of cell in colomn
      allFieldArr = Array.from(enemies[en].parentNode.parentNode.parentNode.childNodes), // field array with colomns   
      currentRow = allFieldArr.indexOf(enemies[en].parentNode.parentNode), // index of colomn
      near = rowArr[current - 4], // cell above tank
      leftBox = allFieldArr[currentRow - 2].childNodes[current - 4], // cell above and to the left
      rightBox = allFieldArr[currentRow + 2].childNodes[current - 4]; // cell above and to the right

      if(rowArr[current - 4].classList.value === 'box' && // checks if no obstruction
        allFieldArr[currentRow - 2].childNodes[current - 4].classList.value === 'box' &&
        allFieldArr[currentRow + 2].childNodes[current - 4].classList.value === 'box') {
          // checks if no player on the way
          if(near.childNodes.length > 0 ? near.childNodes[near.childNodes.length - 1].alt !== 'me' : game.started &&         
          leftBox.childNodes.length > 0 ? leftBox.childNodes[leftBox.childNodes.length - 1].alt !== 'me' : game.started &&
          rightBox.childNodes.length > 0 ? rightBox.childNodes[rightBox.childNodes.length - 1].alt !== 'me' : game.started) {
            // checks if no tank on the way  
            if(near.childNodes.length > 0 ? near.childNodes[near.childNodes.length - 1].alt !== 'enemy' : game.started &&           
            leftBox.childNodes.length > 0 ? leftBox.childNodes[leftBox.childNodes.length - 1].alt !== 'enemy' : game.started &&
            rightBox.childNodes.length > 0 ? rightBox.childNodes[rightBox.childNodes.length - 1].alt !== 'enemy' : game.started) {
              enemies[en].parentNode.previousSibling.previousSibling.append(enemies[en]);
              enemies[en].classList = 'enemy'; // rotates
              window.moveUp = setTimeout(() => { // continues moving
                  if(game.started && enemies[en].parentNode) moveUp(en);
                }, field.difficulty);
            } else changeDirection(en); // if obstruction, changes direction
          } else changeDirection(en); // if obstruction, changes direction
      } else changeDirection(en); // if obstruction, changes direction
    }

    function moveLeft(en) {  
      const rowArr = Array.from(enemies[en].parentNode.parentNode.childNodes),  // colomn array with cells    
      current = rowArr.indexOf(enemies[en].parentNode), // index of cell in colomn
      allFieldArr = Array.from(enemies[en].parentNode.parentNode.parentNode.childNodes), // field array with colomns   
      currentRow = allFieldArr.indexOf(enemies[en].parentNode.parentNode), // index of colomn
      previousRow = allFieldArr[currentRow - 4], // previous row after bullet
      near = previousRow.childNodes[current], // cell near bullet in previous column
      up = previousRow.childNodes[current - 2], // column before bullet, cell above
      down = previousRow.childNodes[current + 2]; // column before bullet, cell below

      if(allFieldArr[currentRow - 4].childNodes[current].classList.value === 'box' && // checks if no obstruction
        allFieldArr[currentRow - 4].childNodes[current + 2].classList.value === 'box' &&
        allFieldArr[currentRow - 4].childNodes[current - 2].classList.value === 'box') {
          // checks if no player on the way
          if(near.childNodes.length > 0 ? near.childNodes[near.childNodes.length - 1].alt !== 'me' : game.started && 
            up.childNodes.length > 0 ? up.childNodes[up.childNodes.length - 1].alt !== 'me' : game.started &&
            down.childNodes.length > 0 ? down.childNodes[down.childNodes.length - 1].alt !== 'me' : game.started) {
            // checks if no tank on the way
            if(near.childNodes.length > 0 ? near.childNodes[near.childNodes.length - 1].alt !== 'enemy' : game.started && 
              up.childNodes.length > 0 ? up.childNodes[up.childNodes.length - 1].alt !== 'enemy' : game.started &&
              down.childNodes.length > 0 ? down.childNodes[down.childNodes.length - 1].alt !== 'enemy' : game.started) {
              allFieldArr[currentRow - 2].childNodes[current].append(enemies[en]); // moves tank on field 
              enemies[en].classList = 'enemy left'; // rotates
              window.moveLeft = setTimeout(() => { // continues moving
                  if(game.started && enemies[en].parentNode) moveLeft(en);
                }, field.difficulty);
            } else changeDirection(en); // if obstruction, changes direction
          } else changeDirection(en); // if obstruction, changes direction
      } else changeDirection(en); // if obstruction, changes direction
    }

    function moveRight(en) {
      const rowArr = Array.from(enemies[en].parentNode.parentNode.childNodes),  // colomn array with cells    
      current = rowArr.indexOf(enemies[en].parentNode), // index of cell in colomn
      allFieldArr = Array.from(enemies[en].parentNode.parentNode.parentNode.childNodes), // field array with colomns
      currentRow = allFieldArr.indexOf(enemies[en].parentNode.parentNode) // index of colomn
      const nextRow = allFieldArr[currentRow + 4], // next row after bullet
      near = nextRow.childNodes[current], // cell near bullet in next column
      up = nextRow.childNodes[current - 2], // next row after bullet, cell above
      down = nextRow.childNodes[current + 2]; // next row after bullet, cell below
       
      if(allFieldArr[currentRow + 4].childNodes[current].classList.value === 'box' && // checks if no obstruction
      allFieldArr[currentRow + 4].childNodes[current + 2].classList.value === 'box' &&
      allFieldArr[currentRow + 4].childNodes[current - 2].classList.value === 'box') {
          // checks if no player on the way
        if(near.childNodes.length > 0 ? near.childNodes[near.childNodes.length - 1].alt !== 'me' : game.started && 
        up.childNodes.length > 0 ? up.childNodes[up.childNodes.length - 1].alt !== 'me' : game.started &&
        down.childNodes.length > 0 ? down.childNodes[down.childNodes.length - 1].alt !== 'me' : game.started) {
            // checks if no tank on the way
          if(near.childNodes.length > 0 ? near.childNodes[near.childNodes.length - 1].alt !== 'enemy' : game.started && 
          up.childNodes.length > 0 ? up.childNodes[up.childNodes.length - 1].alt !== 'enemy' : game.started &&
          down.childNodes.length > 0 ? down.childNodes[down.childNodes.length - 1].alt !== 'enemy' : game.started) {
            enemies[en].classList = 'enemy right'; // rotates
            allFieldArr[currentRow + 2].childNodes[current].append(enemies[en]); // moves tank on field
            window.moveRight = setTimeout(() => { // continues moving
              if(game.started && enemies[en].parentNode) moveRight(en);
            }, field.difficulty);
          } else changeDirection(en); // if obstruction, changes direction
        } else changeDirection(en); // if obstruction, changes direction
      } else changeDirection(en); // if obstruction, changes direction
    }

    function moveDown(en) {
      const rowArr = Array.from(enemies[en].parentNode.parentNode.childNodes),  // colomn array with cells    
      current = rowArr.indexOf(enemies[en].parentNode), // index of cell in colomn
      allFieldArr = Array.from(enemies[en].parentNode.parentNode.parentNode.childNodes), // field array with colomns   
      currentRow = allFieldArr.indexOf(enemies[en].parentNode.parentNode), // index of colomn
      near = rowArr[current + 4], // cell above tanks
      leftBox = allFieldArr[currentRow - 2].childNodes[current + 4], // colomn before, cell above
      rightBox = allFieldArr[currentRow + 2].childNodes[current + 4]; // colomn after, cell below
       
      if(rowArr[current + 4].classList.value === 'box' && // checks if no obstruction
      allFieldArr[currentRow - 2].childNodes[current + 4].classList.value === 'box' &&
      allFieldArr[currentRow + 2].childNodes[current + 4].classList.value === 'box') { 
          // checks if no player on the way
        if(near.childNodes.length > 0 ? near.childNodes[near.childNodes.length - 1].alt !== 'me' : game.started && 
        leftBox.childNodes.length > 0 ? leftBox.childNodes[leftBox.childNodes.length - 1].alt !== 'me' : game.started &&
        rightBox.childNodes.length > 0 ? rightBox.childNodes[rightBox.childNodes.length - 1].alt !== 'me' : game.started) {
            // checks if no tank on the way    
          if(near.childNodes.length > 0 ? near.childNodes[near.childNodes.length - 1].alt !== 'enemy' : game.started &&        
          leftBox.childNodes.length > 0 ? leftBox.childNodes[leftBox.childNodes.length - 1].alt !== 'enemy' : game.started &&
          rightBox.childNodes.length > 0 ? rightBox.childNodes[rightBox.childNodes.length - 1].alt !== 'enemy' : game.started) {
            enemies[en].parentNode.nextSibling.nextSibling.append(enemies[en]); // moves tank on field
            enemies[en].classList = 'enemy down'; // rotates
            window.moveDown = setTimeout(() => { // continues moving
              if(game.started && enemies[en].parentNode) moveDown(en);
            }, field.difficulty);
          } else changeDirection(en); // if obstruction, changes direction
        } else changeDirection(en); // if obstruction, changes direction
      } else changeDirection(en); // if obstruction, changes direction
    }

    function getRandom() { // randomly sorts array and starts moving of enemies
      const arr = [0, 1, 2, 3];
      let randomArr = arr.sort(() => Math.random() - 0.5);
      moveUp(randomArr[0]);
      moveLeft(randomArr[1]);
      moveRight(randomArr[2]);
      moveDown(randomArr[3]);
    }

    function changeDirection(element) { // randomly chooses direction of moving
      const random = Math.random();
      if(random < 0.25) moveUp(element);
      else if(random < 0.50) moveLeft(element);
      else if(random < 0.75) moveRight(element);
      else if(random > 0.75) moveDown(element);
    }

    if(newEn == null) getRandom(); // if first call
    else changeDirection(newEn); // changes direction
  },

  enemiesShooting(en) { // enemies shooting function, en - index of enemy   
    if(game.started) {   
      const me = $('#me'), // player
      enemies = $All('.enemy'), //enemies

      bullet = document.createElement('img'); // creates bullet
      bullet.setAttribute('alt', 'bullet');
      bullet.setAttribute('src', './images/bullet.png');
      bullet.classList = 'bullet';

      const explosion = document.createElement('img'); // creates explosion
      explosion.setAttribute('alt', 'explosion');
      explosion.setAttribute('src', './images/explosion.gif');
      explosion.classList = 'explosion';

      const gunfire = document.createElement('img'); // creates fire
      gunfire.setAttribute('alt', 'gunfire');
      gunfire.setAttribute('src', './images/gunfire.gif');
      gunfire.classList = 'gunfire';

      const rowArr = Array.from(enemies[en].parentNode.parentNode.childNodes),  // colomn array with cells
      current = rowArr.indexOf(enemies[en].parentNode), // index of cell in colomn
      allFieldArr = Array.from(enemies[en].parentNode.parentNode.parentNode.childNodes), // field array with colomns   
      currentRow = allFieldArr.indexOf(enemies[en].parentNode.parentNode); // index of colomn

      if(enemies[en].classList == 'enemy right') {
        bullet.classList = 'bullet right'; // rotates bullet and fire
        gunfire.classList = 'gunfire right';
        function shot(num) { 
          const nextRow = allFieldArr[currentRow + num + 2], // next row after bullet
          near = nextRow.childNodes[current], // cell near in the next colomn
          up = nextRow.childNodes[current - 2], // next row after bullet пули, cell above
          down = nextRow.childNodes[current + 2], // next row after bullet пули, cell below
          upup = nextRow.childNodes[current - 4]; // next row after bullet пули, 2 cells above
          
          if(near.classList.value !== 'box water' && // check if no obstructions
            up.classList.value !== 'box water' &&
            down.classList.value !== 'box water') {
              if(near.classList.value !== 'box' ||
                up.classList.value !== 'box' ||
                down.classList.value !== 'box') {

                // clears interval, shows explosion, hides bullet
                clearInterval(shooting);
                allFieldArr[currentRow + num].childNodes[current].append(explosion);
                bullet.classList = 'hide';
                setTimeout(() => explosion.classList = 'hide', 600);
                
                // SHOT IN WALL
                // check if there is a wall after bullet
                if(near.classList.value === 'box ground' || // checks for wall
                  up.classList.value === 'box ground' ||
                  down.classList.value === 'box ground') {
                  near.classList.value = 'box'; // destroys wall
                  up.classList.value = 'box';
                  down.classList.value = 'box';
                }   
              }
          }            
          // SHOT IN PLAYER'S TANK
          if(near.childNodes.length > 0 ? near.childNodes[near.childNodes.length - 1].alt === 'me' : console.log() || // checks alt value                
            up.childNodes.length > 0 ? up.childNodes[up.childNodes.length - 1].alt === 'me' : console.log() ||
            down.childNodes.length > 0 ? down.childNodes[down.childNodes.length - 1].alt === 'me' : console.log()) {
            $('#lose').play(); // playing sound

            // clears interval, shows explosion, hides bullet
            clearInterval(shooting);
            allFieldArr[currentRow + num].childNodes[current].append(explosion);
            bullet.classList = 'hide';
            setTimeout(() => explosion.classList = 'hide', 600);
            
            allFieldArr[51].childNodes[83].append(me); // recreates player
            field.setLives(); // updates lives
          } 
          // SHOT IN FLAG
          if(near.childNodes.length > 0 ? near.childNodes[0].alt === 'flag' : console.log() || // checks alt value             
            up.childNodes.length > 0 ? up.childNodes[0].alt === 'flag' : console.log() ||
            down.childNodes.length > 0 ? down.childNodes[0].alt === 'flag' : console.log() ||
            upup.childNodes.length > 0 ? upup.childNodes[0].alt === 'flag' : console.log()) {
            field.flagTaken(); // gameover
          }
          allFieldArr[currentRow + num].childNodes[current].append(gunfire); // shows fire
          setTimeout(() => gunfire.classList = 'hide', 200); // hides fire
          allFieldArr[currentRow + num].childNodes[current].append(bullet); // moves bullet on field
        }
        let num = 0; // initial value
        const shooting = setInterval(() => shot(num += 2), 40); // continues shooting
      }

      if(enemies[en].classList == 'enemy left') {
        bullet.classList = 'bullet left'; // rotates bullet and fire
        gunfire.classList = 'gunfire left';
        function shot(num) { 
          const previousRow = allFieldArr[currentRow + num - 2], // column before bullet
          near = previousRow.childNodes[current], // cell near bullet in previous column
          up = previousRow.childNodes[current - 2], // column before bullet, клетка выше пули
          down = previousRow.childNodes[current + 2]; // column before bullet, клетка ниже пули
          
          if(near.classList.value !== 'box water' && // checks if no obstructions
            up.classList.value !== 'box water' &&
            down.classList.value !== 'box water') {
              if(near.classList.value !== 'box' ||
                up.classList.value !== 'box' ||
                down.classList.value !== 'box') {
                // clears interval, shows explosion, hides bullet
                clearInterval(shooting);
                allFieldArr[currentRow + num].childNodes[current].append(explosion);
                bullet.classList = 'hide';
                setTimeout(() => explosion.classList = 'hide', 600);
                
                // SHOT IN WALL
                // check if there is a wall after bullet
                if(near.classList.value === 'box ground' || // checks for wall             
                  up.classList.value === 'box ground' ||
                  down.classList.value === 'box ground') {
                  near.classList.value = 'box'; // destroyes wall      
                  up.classList.value = 'box';
                  down.classList.value = 'box';
                }   
              }
          }            
          // SHOT IN PLAYER'S TANK
          if(near.childNodes.length > 0 ? near.childNodes[near.childNodes.length - 1].alt === 'me' : console.log() || // checks alt value              
            up.childNodes.length > 0 ? up.childNodes[up.childNodes.length - 1].alt === 'me' : console.log() ||
            down.childNodes.length > 0 ? down.childNodes[down.childNodes.length - 1].alt === 'me' : console.log()) {
            $('#lose').play(); // playing sound

            // clears interval, shows explosion, hides bullet
            clearInterval(shooting);
            allFieldArr[currentRow + num].childNodes[current].append(explosion);
            bullet.classList = 'hide';
            setTimeout(() => explosion.classList = 'hide', 600);

            allFieldArr[51].childNodes[83].append(me); // recreates player
            field.setLives(); // update lives
          } 
          // SHOT IN FLAG
          if(near.childNodes.length > 0 ? near.childNodes[0].alt === 'flag' : console.log() || // checks alt value             
            up.childNodes.length > 0 ? up.childNodes[0].alt === 'flag' : console.log() ||
            down.childNodes.length > 0 ? down.childNodes[0].alt === 'flag' : console.log()) {
            field.flagTaken(); // game over
          }
          if(currentRow + num - 6 > 0) { // for shot from the right
            if(allFieldArr[currentRow + num - 6].childNodes[current - 4].childNodes.length > 0 ? 
              allFieldArr[currentRow + num - 6].childNodes[current - 4].childNodes[0].alt === 'flag' : console.log()) {
              field.flagTaken(); // game over
            }
          }
          allFieldArr[currentRow + num].childNodes[current].append(bullet); // moves bullet on field
        }
        let num = 0; // initial value
        const shooting = setInterval(() => shot(num -= 2), 40); // continues shooting
      }

      if(enemies[en].classList == 'enemy') {
        bullet.classList = 'bullet'; // rotates bullet and fire
        gunfire.classList = 'gunfire';
        function shot(num) { 
          const near = rowArr[current + num - 2],
          leftBox = allFieldArr[currentRow - 2].childNodes[current + num - 2],
          rightBox = allFieldArr[currentRow + 2].childNodes[current + num - 2];
          
          if(near.classList.value !== 'box water' && // checks for obstructions
            leftBox.classList.value !== 'box water' &&
            rightBox.classList.value !== 'box water') {
              if(near.classList.value !== 'box' ||
                leftBox.classList.value !== 'box' ||
                rightBox.classList.value !== 'box') {

                // clears interval, shows explosion, hides bullet
                clearInterval(shooting);
                rowArr[current + num].append(explosion);
                bullet.classList = 'hide';
                setTimeout(() => explosion.classList = 'hide', 600);
                
                // SHOT IN WALL
                // check if there is a wall after bullet
                if(near.classList.value === 'box ground' || // checks for wall             
                  leftBox.classList.value === 'box ground' ||
                  rightBox.classList.value === 'box ground') {
                  near.classList.value = 'box'; // destroys wall       
                  leftBox.classList.value = 'box';
                  rightBox.classList.value = 'box';
                }   
              }
          }            
          // SHOT IN PLAYER'S TANK
          if(near.childNodes.length > 0 ? near.childNodes[near.childNodes.length - 1].alt === 'me' : console.log() || // checks alt value             
            leftBox.childNodes.length > 0 ? leftBox.childNodes[leftBox.childNodes.length - 1].alt === 'me' : console.log() ||
            rightBox.childNodes.length > 0 ? rightBox.childNodes[rightBox.childNodes.length - 1].alt === 'me' : console.log()) {
            $('#lose').play(); // playing sound

            // clears interval, shows explosion, hides bullet
            clearInterval(shooting);
            rowArr[current + num].append(explosion);
            bullet.classList = 'hide';
            setTimeout(() => explosion.classList = 'hide', 600);

            allFieldArr[51].childNodes[83].append(me); // recreates player
            field.setLives(); // updates lives
          } 
          rowArr[current + num].append(bullet); // moves bullet on field
        }
        let num = 0; // initial value
        const shooting = setInterval(() => shot(num -= 2), 40);// continue shooting
      }

      if(enemies[en].classList == 'enemy down') {
        bullet.classList = 'bullet down'; // rotates bullet and fire
        gunfire.classList = 'gunfire down';
        function shot(num) { 
          const near = rowArr[current + num + 2], // cell after bullet
          leftBox = allFieldArr[currentRow - 2].childNodes[current + num + 2], // cell after and to the left
          rightBox = allFieldArr[currentRow + 2].childNodes[current + num + 2], // cell after and to the right
          leftleftBox = allFieldArr[currentRow - 4].childNodes[current + num + 2]; // cell after and 2 cells to the left
          
          if(near.classList.value !== 'box water' && // checks if no obstructions
            leftBox.classList.value !== 'box water' &&
            rightBox.classList.value !== 'box water') {
              if(near.classList.value !== 'box' ||
                leftBox.classList.value !== 'box' ||
                rightBox.classList.value !== 'box') {

                // shows explosion and hides bullet
                clearInterval(shooting);
                rowArr[current + num].append(explosion);
                bullet.classList = 'hide';
                setTimeout(() => explosion.classList = 'hide', 600);
                
                // SHOT IN WALL
                // check if there is a wall after bullet
                if(near.classList.value === 'box ground' ||               
                  leftBox.classList.value === 'box ground' ||
                  rightBox.classList.value === 'box ground') {
                  near.classList.value = 'box';           
                  leftBox.classList.value = 'box';
                  rightBox.classList.value = 'box';
                }   
              }
          }            
          // SHOT IN PLAYER'S TANK
          if(near.childNodes.length > 0 ? near.childNodes[near.childNodes.length - 1].alt === 'me' : console.log() || // checks alt value              
            leftBox.childNodes.length > 0 ? leftBox.childNodes[leftBox.childNodes.length - 1].alt === 'me' : console.log() ||
            rightBox.childNodes.length > 0 ? rightBox.childNodes[rightBox.childNodes.length - 1].alt === 'me' : console.log()) {
            $('#lose').play(); // playing sound

            // shows explosion and hides bullet
            clearInterval(shooting);
            rowArr[current + num].append(explosion);
            bullet.classList = 'hide';
            setTimeout(() => explosion.classList = 'hide', 600);

            // recreates player's tank
            allFieldArr[51].childNodes[83].append(me);
            field.setLives(); // updates lives
          } 
          // SHOT IN FLAG
          if(near.childNodes.length > 0 ? near.childNodes[0].alt === 'flag' : console.log() || // checks alt value          
            leftBox.childNodes.length > 0 ? leftBox.childNodes[0].alt === 'flag' : console.log() ||
            rightBox.childNodes.length > 0 ? rightBox.childNodes[0].alt === 'flag' : console.log() ||
            leftleftBox.childNodes.length > 0 ? leftleftBox.childNodes[0].alt === 'flag' : console.log()) {
            field.flagTaken(); // game over
          }
          rowArr[current + num].append(bullet); // moves bullet on field
        }
        let num = 0 // initial value
        const shooting = setInterval(() => shot(num += 2), 40); // continue shooting
      }
    }
           
    const timeOut = Math.random() * (field.difficulty * 20 + 1000);
    setTimeout(() => this.enemiesShooting(en), timeOut);
  },

  startShooting() { // function for enemies' random begining of shooting   
      const timeOut1 = Math.random() * 5000
      setTimeout(() => this.enemiesShooting(0), timeOut1);

      const timeOut2 = Math.random() * 5000;
      setTimeout(() => this.enemiesShooting(1), timeOut2);

      const timeOut3 = Math.random() * 5000;
      setTimeout(() => this.enemiesShooting(2), timeOut3);
          
      const timeOut4 = Math.random() * 5000;
      setTimeout(() => this.enemiesShooting(3), timeOut4);
  }
}

$('button').addEventListener('click', () => field.createField()); // creates field and starts game

// Mobile version controls
$('#up').addEventListener('touchstart', game.moveUp);
$('#down').addEventListener('touchstart', game.moveDown);
$('#left').addEventListener('touchstart', game.moveLeft);
$('#right').addEventListener('touchstart', game.moveRight);
$('#ishot').addEventListener('touchstart', () => {
  if(game.shotTimeEnded) {
    $('#shot').play(); // playing sound
    game.iShot();

    game.shotTimeEnded = false;
    setTimeout(() => game.shotTimeEnded = true, 800);
  }
})

field.setHighscores(); // set highscore when page is loading
