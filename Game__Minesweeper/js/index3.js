let once = 0        

document.querySelector('#plus').onclick = function() {
    document.querySelector('#size').innerHTML++
    game.width++
    game.height++
    game.minesAmount = parseInt(game.width * game.height / 6)
    game.flags = 0  
    page.init()
    page.init()
}

document.querySelector('#minus').onclick = function() {
    document.querySelector('#size').innerHTML--
    game.width--
    game.height--
    game.minesAmount = parseInt(game.width * game.height / 6)
    game.flags = 0
    page.init()
    page.init()
}

class Box {
    constructor() {
        this.mine = false
        this.mineAround = 0
        this.opened = false
        this.flag = false
        this.flagsAround = 0
    }
}

const game = {
    width: 8,
    height: 8,
    minesAmount: 10,
    minesOpened: 0,
    flags: 0,

    field: [],

    setMines: function() {
        this.field = []

        for(a = 0; a < this.width; a++) {
            const arr = []
            for(b = 0; b < this.height; b++) {
                arr.push(new Box())
            }
            this.field.push(arr)
        }

        for(a = 0; a < this.minesAmount; ) {
            let x = parseInt(Math.random() * this.width - 0.0001)
            let y = parseInt(Math.random() * this.height - 0.0001)
            if(!(this.field[x][y].mine)) {
                this.field[x][y].mine = true
                a++
            }
        }
    },

    mineAroundCount: function(x, y) {
        let xStart = x > 0 ? x - 1 : x
        let yStart = y > 0 ? y - 1 : y
        let xEnd = x < this.width - 1 ? x + 1 : x
        let yEnd = y < this.height - 1 ? y + 1 : y
        let count = 0
        for(c = xStart; c <= xEnd; c++) {
            for(d = yStart; d <= yEnd; d++) {
                if(this.field[c][d].mine && !(x == c && y == d)) count++
            }
        }
        this.field[x][y].mineAround = count
    },

    flagsAroundCount: function(x, y) {
        let xStart = x > 0 ? x - 1 : x
        let yStart = y > 0 ? y - 1 : y
        let xEnd = x < this.width - 1 ? x + 1 : x
        let yEnd = y < this.height - 1 ? y + 1 : y
        let count = 0
        for(k = xStart; k <= xEnd; k++) {
            for(l = yStart; l <= yEnd; l++) {
                if(this.field[k][l].flag && !(x == k && y == l)) count++
            }
        }
        this.field[x][y].flagsAround = count
    },

    startMAC: function() {
        for(a = 0; a < this.width; a++) {
            for(b = 0; b < this.height; b++) {
                this.mineAroundCount(a, b)
            }
        }
    },

    start: function() {
        this.minesOpened = 0
        this.setMines()
        this.startMAC()
    }
}

const page = {
    init: function() {
        this.gameInterface.init()
    },

    gameInterface: {
        table: null,

        init: function() {
            game.start()
            this.div = document.querySelector('.field')
            const self = this
            this.drawField()
            this.div.addEventListener('click', function(e) {
                if(e.target.matches('td') && !(e.target.classList.contains('lock'))) self.open(e)
            })
            this.div.addEventListener('contextmenu', function(e) {
                if(e.target.matches('td')) {
                    self.lock(e)
                }
            })
            this.div.addEventListener('dblclick', function(e) {
                if(e.target.matches('td')) {
                    self.doubleClick(e)
                }
            })
        },

        drawField: function() {
            this.div.innerHTML = ''
            const table = document.createElement('table')
            this.table = table
            for(a = 0; a < game.height; a++) {
                let tr = document.createElement('tr')
                for(b = 0; b < game.width; b++) {
                    let td = document.createElement('td')
                    tr.append(td)
                }
                table.append(tr)
            }
            this.div.append(table)
            const parag = document.querySelector('p')
            parag.innerHTML = `${game.flags} / ${game.minesAmount}`
        },
        
        open: function(e) {
            if(once === 0) {
                let button = document.createElement('button')
                button.innerHTML = 'Restart'
                const pageDiv = document.querySelector('.page')
                pageDiv.prepend(button)
                button.onclick = function() {
                    window.location.reload()
                }
                once++
            }

            x = e.target.cellIndex
            y = e.target.parentNode.rowIndex

            this.openAround(x, y)
        },

        doubleClick: function(e) {
            x = e.target.cellIndex
            y = e.target.parentNode.rowIndex
            game.flagsAroundCount(x, y)   

            if(game.field[x][y].flagsAround == game.field[x][y].mineAround) {
                let startX = x > 0 ? x - 1 : x
                let startY = y > 0 ? y - 1 : y
                let endX = x < game.width - 1 ? x + 1 : x
                let endY = y < game.height - 1 ? y + 1 : y
                for(g = startX; g <= endX; g++) {
                    for(f = startY; f <= endY; f++) {
                        let td = this.table.rows[f].children[g]
                        if(!(td.classList.contains('lock'))) {
                            td.innerHTML = game.field[g][f].mineAround
                            game.field[g][f].opened = true
                            if(!(td.classList.contains('open'))) { game.minesOpened++}                            
                            td.classList.add('open')
                        }
                    }
                }
            }
            
            if(game.width * game.height - game.minesAmount == game.minesOpened) {
                document.querySelector('.end2').className = 'win'
            }
            
        },
        
        openAround: function(x, y) {
            let td = this.table.rows[y].children[x]

            if(game.field[x][y].opened) return

            if(game.field[x][y].mine) {
                td.style.background = 'gray'
                td.innerHTML = 'X'
                td.classList = 'open'
                for(g = 0; g < game.width; g++) {
                    for(f = 0; f < game.height; f++) {
                        if(game.field[g][f].mine) {                           
                            let bomb = this.table.rows[f].children[g]
                            bomb.style.background = 'gray'
                            bomb.innerHTML = 'X'
                            bomb.classList = 'open'
                        }
                    }
                }
                document.querySelector('.end').className = 'over'
            } else {
                td.innerHTML = game.field[x][y].mineAround
                game.field[x][y].opened = true
                if(!(td.classList.contains('open'))) { game.minesOpened++}
                td.classList.add('open')
                if(game.field[x][y].mineAround == 0) {
                    let startX = x > 0 ? x - 1 : x
                    let startY = y > 0 ? y - 1 : y
                    let endX = x < game.width - 1 ? x + 1 : x
                    let endY = y < game.height - 1 ? y + 1 : y
                    for(g = startX; g <= endX; g++) {
                        for(f = startY; f <= endY; f++) {
                            let td = this.table.rows[f].children[g]
                            td.innerHTML = game.field[g][f].mineAround
                            game.field[g][f].opened = true
                            if(!(td.classList.contains('open'))) { game.minesOpened++}
                            td.classList.add('open')
                            if(game.field[g][f].mineAround == 0) {
                                let startX = g > 0 ? g - 1 : g
                                let startY = f > 0 ? f - 1 : f
                                let endX = g < game.width - 1 ? g + 1 : g
                                let endY = f < game.height - 1 ? f + 1 : f
                                for(u = startX; u <= endX; u++) {
                                    for(i = startY; i <= endY; i++) {
                                        let td = this.table.rows[i].children[u]
                                        td.innerHTML = game.field[u][i].mineAround
                                        game.field[u][i].opened = true
                                        if(!(td.classList.contains('open'))) { game.minesOpened++}
                                        td.classList.add('open')
                                        if(game.field[u][i].mineAround == 0) {
                                            let startX = u > 0 ? u - 1 : u
                                            let startY = i > 0 ? i - 1 : i
                                            let endX = u < game.width - 1 ? u + 1 : u
                                            let endY = i < game.height - 1 ? i + 1 : i
                                            for(q = startX; q <= endX; q++) {
                                                for(w = startY; w <= endY; w++) {
                                                    let td = this.table.rows[w].children[q]
                                                    td.innerHTML = game.field[q][w].mineAround
                                                    game.field[q][w].opened = true
                                                    if(!(td.classList.contains('open'))) { game.minesOpened++}
                                                    td.classList.add('open')
                                                    if(game.field[q][w].mineAround == 0) {
                                                        let startX = q > 0 ? q - 1 : q
                                                        let startY = w > 0 ? w - 1 : w
                                                        let endX = q < game.width - 1 ? q + 1 : q
                                                        let endY = w < game.height - 1 ? w + 1 : w
                                                        for(z = startX; z <= endX; z++) {
                                                            for(c = startY; c <= endY; c++) {
                                                                let td = this.table.rows[c].children[z]
                                                                td.innerHTML = game.field[z][c].mineAround
                                                                game.field[z][c].opened = true
                                                                if(!(td.classList.contains('open'))) { game.minesOpened++}
                                                                td.classList.add('open')
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

                if(game.width * game.height - game.minesAmount == game.minesOpened) {
                    document.querySelector('.end2').className = 'win'
                }
            }
        },

        lock: function(e) {
            x = e.target.cellIndex
            y = e.target.parentNode.rowIndex
            const parag = document.querySelector('p')

            if(game.field[x][y].opened) return
            if(!(game.field[x][y].flag) && game.flags < game.minesAmount) {
                e.target.classList.add('lock')
                game.field[x][y].flag = true
                parag.innerHTML = `${++game.flags} / ${game.minesAmount}`
            } else if(game.field[x][y].flag) {
                e.target.classList.remove('lock')
                game.field[x][y].flag = false
                parag.innerHTML = `${--game.flags} / ${game.minesAmount}`
            }

            if(game.width * game.height - game.minesAmount == game.minesOpened) {
                document.querySelector('.end2').className = 'win'
            }

            e.preventDefault()
        }
    }
}

window.onload = function() {
    page.init()
    const restart = document.querySelectorAll('.restart')
    restart.forEach(button => button.onclick = function() {
        window.location.reload()
    })
}