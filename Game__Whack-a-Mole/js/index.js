const preview = document.querySelector('#preview'),
game = document.querySelector('#game'),
button = document.querySelector('#start'),
stats = document.querySelector('#stats'),
select = document.querySelector('#difficulty'),
selectAgain = document.querySelector('#difficultyAgain'),
buttonAgain = document.querySelector('#startAgain'),
time = document.querySelector('#time'),
got = document.querySelector('#got'),
missed = document.querySelector('#missed'),
end = document.querySelector('#end'),
final = document.querySelector('#final')

// audio

boom = document.querySelector('#boom')
win = document.querySelector('#win')
lose = document.querySelector('#lose')
oi = document.querySelector('#oi')

let goOn, timer, speed // таймеры и скорость

// Создаю класс ячейки

class Box {
    constructor() {
        this.opened = false
    }
}

const page = {
    field: [],    
    
    // значения вверху игры
    green: 0, 
    red: 0, 
    seconds: 0,

    // Создаю анимацию курсора при клике

    cursorArray: ['url("./images/cursor1.png"), auto', // массив со значениями атрибута cursor
                'url("./images/cursor2.png"), auto',
                'url("./images/cursor3.png"), auto',
                'url("./images/cursor2.png"), auto',
                'url("./images/cursor1.png", auto'],

    i: 0,
    cursor: function() {
        if(page.i === 0) boom.play() // звук удара
        game.style.cursor = page.cursorArray[page.i] // задаю по очереди все значения с таймаутом 80 мс
        page.i++
        const cursorAnimation = setTimeout(page.cursor, 80)    
        if(page.i == page.cursorArray.length) { // чтобы выполнялось один раз, удаляю таймаут после прохождения массива
            clearTimeout(cursorAnimation)
            game.style.cursor = page.cursorArray[0]
            page.i = 0
        }
    },
    
    // При клике на кнопку "Начать игру":

    firstStart: function() {
        preview.classList = 'hide' // скрываются ненужные и отображаются скрытые элементы 
        stats.classList = ''
        game.classList = ''
        this.field = [] // массив для рядов объктове
        for(a = 0; a < 10; a++) { // создаются ячейки таблицы
            let tr = document.createElement('tr')
            let arr = [] // массив для объектов ячеек
            for(b = 0; b < 10; b++) {
                let td = document.createElement('td')
                tr.append(td)
                arr.push(new Box()) // объекты ячеек
            }
            game.append(tr)
            this.field.push(arr) 
            page.field = this.field
        }
        speed = select.options[select.selectedIndex].value // скорость берется с элемента select
        goOn = setInterval(fullGame.playing, speed) // запускается функция и промежутком с select
        timer = setInterval(() => { // секундомер вверху
            page.seconds++
            time.innerHTML = page.seconds
        }, 1000)
    },

    // После окончания игры при клике на кнопку "Сыграть еще":

    nextStart: function() {
        end.classList = 'hide' // скрывается контейнер с информацией о победе
        final.classList = 'hide'
        page.green = 0 // все обнуляется 
        page.red = 0
        page.seconds = 0
        got.innerHTML = page.green
        missed.innerHTML = page.red
        time.innerHTML = page.seconds
        game.innerHTML = '' // заново создается таблица
        this.field = [] // массив для рядов объектов
        for(a = 0; a < 10; a++) {
            let tr = document.createElement('tr')
            let arr = [] // массив для объектов ячеек
            for(b = 0; b < 10; b++) {
                let td = document.createElement('td')
                tr.append(td)
                arr.push(new Box()) // объекты ячеек
            }
            game.append(tr)
            this.field.push(arr)
            page.field = this.field
        }
        speed = selectAgain.options[selectAgain.selectedIndex].value // скорость берется с элемента select
        goOn = setInterval(fullGame.playing, speed) // запускается функция с промежутком с select
        timer = setInterval(() => { // секундомер вверху
            page.seconds++
            time.innerHTML = page.seconds
        }, 1000)
    }
}

// При клике во области игры запускается анимация

game.addEventListener('click', page.cursor)

// добавление событий к кнопкам "Начать игру"

button.addEventListener('click', page.firstStart)
buttonAgain.addEventListener('click', page.nextStart)

const fullGame = {

    // по клику делает ячейку зеленой:

    tdClick: function(ev) { // при клике, если еще на красная, становится зеленой 
        if(this.classList != 'red showOverflow') {
            this.classList = 'green showOverflow'
            this.innerHTML = '<img class="yesno" src="./images/yes.png" alt="yes">'
            page.green++
            got.innerHTML = page.green
            oi.play() // звук ой
            
            if(page.green > 49) { // для завершения игры
                end.classList = ''
                final.classList = ''                    
                document.querySelector('h2').innerHTML = 'Вы выиграли! Еще раз?'
                clearInterval(goOn)
                clearInterval(timer)
                win.play() // звук победы
            }
        }
    },

    // делает ячейку красной:

    blueToRed: function() {
        if(this.classList == 'blue showOverflow') {
            if(page.red > 49) { // для завершения игры
                end.classList = ''
                final.classList = ''
                document.querySelector('h2').innerHTML = 'Вы проиграли! Еще раз?'
                clearInterval(timer)
                clearInterval(goOn)
                lose.play() // звук поражения
            } else { // делает ячейку красной
                this.classList = 'red showOverflow'
                this.innerHTML = '<img class="yesno" src="./images/no.png" alt="no">'
                page.red++
                missed.innerHTML = page.red
            }
        }
    },

    // Функция, которая открывает случайную ячейку

    playing: function () {
        x = Math.floor(Math.random() * 10 - 0.01) // случайные значения для индекса
        y = Math.floor(Math.random() * 10 - 0.01)

        let td = game.rows[y].children[x] // элемент td со случайными значениями индекса

        if(page.field[x][y].opened) { // если ячейка уже открыта, начинает заново
            fullGame.playing()
        } else {
            td.classList = 'blue' // делает ячейку синей и показывает крота
            td.innerHTML = '<img class="mole" src="./images/icon.png" alt="mole">'
            setTimeout(function() { td.classList += ' showOverflow' }, 150) // показывает оверфлов, чтобы крота было нормально видно
            td.addEventListener('click', fullGame.tdClick)
            setTimeout(fullGame.blueToRed.bind(td), speed) // задает время за которое нужно успеть нажать, потом делает ячейку красной
            page.field[x][y].opened = true
        }
    }
}