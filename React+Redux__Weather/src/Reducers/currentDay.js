const currentDayReducer = (state = {}, action) => {
  if(action.type === 'CHANGE_DAY') {
    const { day } = action.payload;
    state = { 
      currentDay: day,
    }
    return state;
  } else {
    return state;
  }
}

export default currentDayReducer;