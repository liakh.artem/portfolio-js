const weatherReducer = (state = {}, action) => {
    switch (action.type) {
      case 'NEW_DATA': { 
        const { data } = action.payload;
        state = {
        city: data.city.name,
        country: data.city.country,
        today: {
            a00: data.list[0],
            a03: data.list[1],
            a06: data.list[2],
            a09: data.list[3],
            a12: data.list[4],
            a15: data.list[5],
            a18: data.list[6],
            a21: data.list[7],
        },
        tommorrow: {
            a00: data.list[8],
            a03: data.list[9],
            a06: data.list[10],
            a09: data.list[11],
            a12: data.list[12],
            a15: data.list[13],
            a18: data.list[14],
            a21: data.list[15],
        },
        tommorrow2: {
            a00: data.list[16],
            a03: data.list[17],
            a06: data.list[18],
            a09: data.list[19],
            a12: data.list[20],
            a15: data.list[21],
            a18: data.list[22],
            a21: data.list[23],
        },
        tommorrow3: {
            a00: data.list[24],
            a03: data.list[25],
            a06: data.list[26],
            a09: data.list[27],
            a12: data.list[28],
            a15: data.list[29],
            a18: data.list[30],
            a21: data.list[31],
        },
        tommorrow4: {
            a00: data.list[32],
            a03: data.list[33],
            a06: data.list[34],
            a09: data.list[35],
            a12: data.list[36],
            a15: data.list[37],
            a18: data.list[38],
            a21: data.list[39],
        },
    }
    return state;
    }
    default: return state;
}
}

export default weatherReducer;