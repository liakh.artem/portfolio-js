import { combineReducers } from 'redux';
import weatherReducer from './weather';
import currentDayReducer from './currentDay';
import { reducer as formReducer } from 'redux-form'

export default combineReducers({
    weatherReducer,
    currentDayReducer,
    form: formReducer,
});