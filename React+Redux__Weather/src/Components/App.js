import React from 'react';
import { connect } from 'react-redux';
import '../css/App.css';
import Weather from './Weather';
import Title from './Title';
import Form from './Form';

const mapStateToProps = ({ weatherReducer }) => {
  const { city, country, today, tommorow, tommorow2, tommorow3, tommorow4 } = weatherReducer,
    props = { city, country, today, tommorow, tommorow2, tommorow3, tommorow4 };
  return props;
};

class App extends React.Component {
  render() {
    return(
      <div>
        <Title />
        <Form />
        { this.props.today && <Weather/> }
      </div>
    );
  }
}

export default connect(mapStateToProps)(App);
