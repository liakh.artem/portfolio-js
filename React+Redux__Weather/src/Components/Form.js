import React from 'react';
import { reduxForm, Field } from 'redux-form';
import { fetchWeather } from '../Actions/getWeatherAction';
import store from '../index';

const getWeather = (values) => {
    store.dispatch(fetchWeather(values.city, values.country));
}

const Form = ({ handleSubmit }) => {
    return (
        <form onSubmit={handleSubmit} className="mainform" >
            <Field component="input" required name="city" placeholder="Kiev..." />
            <Field component="input" required name="country" placeholder="Ukraine..." /><br />
            <input type="submit" value="Get Forecast" />
        </form>
    )
}

export default reduxForm({
    form: 'CityCountry',
    onSubmit: getWeather,
})(Form);