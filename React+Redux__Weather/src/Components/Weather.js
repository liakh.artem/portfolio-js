import React from 'react';
import { connect } from 'react-redux';
import { currentDayAction } from '../Actions/currentDayAction';

const mapStateToProps = ({ weatherReducer, currentDayReducer }) => {
    const { city, country, today, tommorrow, tommorrow2, tommorrow3, tommorrow4 } = weatherReducer,
      { currentDay } = currentDayReducer,
      props = { city, country, today, tommorrow, tommorrow2, tommorrow3, tommorrow4, currentDay };
    return props;
};

class Weather extends React.Component {
  render() {
    const { city, country, today, tommorrow, tommorrow2, tommorrow3, tommorrow4, currentDay, dispatch } = this.props,
      changeDay = day => dispatch(currentDayAction(day));
    return(
      <div>
      <h3>Weather in {city}, {country}</h3>
      <div className="tabs">
        <div onClick={() => changeDay(today)} className={ currentDay === today ? "currentTab" : "tab" }>
            <p>{today.a00 && today.a00.dt_txt.split(' ')[0]}</p>
            <img className="icon" src={today.a00 && `../img/${today.a00.weather[0].main}.png`} alt="weather image" />
            <p>Temp: {today.a00 && today.a00.main.temp} °C</p>
        </div>
        <div onClick={() => changeDay(tommorrow)} className={ currentDay === tommorrow ? "currentTab" : "tab" }>
            <p>{tommorrow.a00 && tommorrow.a00.dt_txt.split(' ')[0]}</p>
            <img className="icon" src={tommorrow.a00 && `../img/${tommorrow.a00.weather[0].main}.png`} alt="weather image" />
            <p>Temp: {tommorrow.a00 && tommorrow.a00.main.temp} °C</p>
        </div>
        <div onClick={() => changeDay(tommorrow2)} className={ currentDay === tommorrow2 ? "currentTab" : "tab" }>
            <p>{tommorrow2.a00 && tommorrow2.a00.dt_txt.split(' ')[0]}</p>
            <img className="icon" src={tommorrow2.a00 && `../img/${tommorrow2.a00.weather[0].main}.png`} alt="weather image" />
            <p>Temp: {tommorrow2.a00 && tommorrow2.a00.main.temp} °C</p>
        </div>
        <div onClick={() => changeDay(tommorrow3)} className={ currentDay === tommorrow3 ? "currentTab" : "tab" }>
            <p>{tommorrow3.a00 && tommorrow3.a00.dt_txt.split(' ')[0]}</p>
            <img className="icon" src={tommorrow3.a00 && `../img/${tommorrow3.a00.weather[0].main}.png`} alt="weather image" />
            <p>Temp: {tommorrow3.a00 && tommorrow3.a00.main.temp} °C</p>
        </div>
        <div onClick={() => changeDay(tommorrow4)} className={ currentDay === tommorrow4 ? "currentTab" : "tab" }>
            <p>{tommorrow4.a00 && tommorrow4.a00.dt_txt.split(' ')[0]}</p>
            <img className="icon" src={tommorrow4.a00 && `../img/${tommorrow4.a00.weather[0].main}.png`} alt="weather image" />
            <p>Temp: {tommorrow4.a00 && tommorrow4.a00.main.temp} °C</p>
        </div>
      </div>
      <div className="table">
        <div className="table__headings">
            <p>Temperature:</p>
            <p>Feels like:</p>
            <p>Pressure:</p>
            <p>Humidity:</p>
            <p>Wind:</p>
        </div>
        <div>
            <p>{currentDay && currentDay.a00.dt_txt.split(' ')[1]}</p>
            <img className="icon" src={currentDay && `../img/${currentDay.a00.weather[0].main}.png`} alt="weather icon"></img>
            <p>{currentDay && currentDay.a00.main.temp} °C</p>
            <p>{currentDay && currentDay.a00.main.feels_like} °C</p>
            <p>{currentDay && currentDay.a00.main.pressure}</p>
            <p>{currentDay && currentDay.a00.main.humidity}</p>
            <p>{currentDay && currentDay.a00.wind.speed} m/s</p>
        </div>
        <div>
            <p>{currentDay && currentDay.a03.dt_txt.split(' ')[1]}</p>
            <img className="icon" src={currentDay && `../img/${currentDay.a03.weather[0].main}.png`} alt="weather icon"></img>
            <p>{currentDay && currentDay.a03.main.temp} °C</p>
            <p>{currentDay && currentDay.a03.main.feels_like} °C</p>
            <p>{currentDay && currentDay.a03.main.pressure}</p>
            <p>{currentDay && currentDay.a03.main.humidity}</p>
            <p>{currentDay && currentDay.a03.wind.speed} m/s</p>
        </div>
        <div>
            <p>{currentDay && currentDay.a06.dt_txt.split(' ')[1]}</p>
            <img className="icon" src={currentDay && `../img/${currentDay.a06.weather[0].main}.png`} alt="weather icon"></img>
            <p>{currentDay && currentDay.a06.main.temp} °C</p>
            <p>{currentDay && currentDay.a06.main.feels_like} °C</p>
            <p>{currentDay && currentDay.a06.main.pressure}</p>
            <p>{currentDay && currentDay.a06.main.humidity}</p>
            <p>{currentDay && currentDay.a06.wind.speed} m/s</p>
        </div>
        <div>
            <p>{currentDay && currentDay.a09.dt_txt.split(' ')[1]}</p>
            <img className="icon" src={currentDay && `../img/${currentDay.a09.weather[0].main}.png`} alt="weather icon"></img>
            <p>{currentDay && currentDay.a09.main.temp} °C</p>
            <p>{currentDay && currentDay.a09.main.feels_like} °C</p>
            <p>{currentDay && currentDay.a09.main.pressure}</p>
            <p>{currentDay && currentDay.a09.main.humidity}</p>
            <p>{currentDay && currentDay.a09.wind.speed} m/s</p>
        </div>
        <div>
            <p>{currentDay && currentDay.a12.dt_txt.split(' ')[1]}</p>
            <img className="icon" src={currentDay && `../img/${currentDay.a12.weather[0].main}.png`} alt="weather icon"></img>
            <p>{currentDay && currentDay.a12.main.temp} °C</p>
            <p>{currentDay && currentDay.a12.main.feels_like} °C</p>
            <p>{currentDay && currentDay.a12.main.pressure}</p>
            <p>{currentDay && currentDay.a12.main.humidity}</p>
            <p>{currentDay && currentDay.a12.wind.speed} m/s</p>
        </div>
        <div>
            <p>{currentDay && currentDay.a15.dt_txt.split(' ')[1]}</p>
            <img className="icon" src={currentDay && `../img/${currentDay.a15.weather[0].main}.png`} alt="weather icon"></img>
            <p>{currentDay && currentDay.a15.main.temp} °C</p>
            <p>{currentDay && currentDay.a15.main.feels_like} °C</p>
            <p>{currentDay && currentDay.a15.main.pressure}</p>
            <p>{currentDay && currentDay.a15.main.humidity}</p>
            <p>{currentDay && currentDay.a15.wind.speed} m/s</p>
        </div>
        <div>
            <p>{currentDay && currentDay.a18.dt_txt.split(' ')[1]}</p>
            <img className="icon" src={currentDay && `../img/${currentDay.a18.weather[0].main}.png`} alt="weather icon"></img>
            <p>{currentDay && currentDay.a18.main.temp} °C</p>
            <p>{currentDay && currentDay.a18.main.feels_like} °C</p>
            <p>{currentDay && currentDay.a18.main.pressure}</p>
            <p>{currentDay && currentDay.a18.main.humidity}</p>
            <p>{currentDay && currentDay.a18.wind.speed} m/s</p>
        </div>
        <div>
            <p>{currentDay && currentDay.a21.dt_txt.split(' ')[1]}</p>
            <img className="icon" src={currentDay && `../img/${currentDay.a21.weather[0].main}.png`} alt="weather icon"></img>
            <p>{currentDay && currentDay.a21.main.temp} °C</p>
            <p>{currentDay && currentDay.a21.main.feels_like} °C</p>
            <p>{currentDay && currentDay.a21.main.pressure}</p>
            <p>{currentDay && currentDay.a21.main.humidity}</p>
            <p>{currentDay && currentDay.a21.wind.speed} m/s</p>
        </div>
      </div>
      </div>
    );
  }
}

export default connect(mapStateToProps)(Weather);