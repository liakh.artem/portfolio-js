import React from 'react';

class Title extends React.Component {
  render() {
    return(
      <div className="header">
        <h1>Current & Forecast weather</h1>
        <p>Forecast is available at any location or city</p>
      </div>
    );
  }
}

export default Title;