export const currentDayAction = (day) => ({
    type: 'CHANGE_DAY',
    payload: {
        day,
    },
});