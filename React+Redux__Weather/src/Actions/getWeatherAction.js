import { currentDayAction } from './currentDayAction';

export const getWeatherAction = (data) => ({
    type: 'NEW_DATA',
    payload: {
      data,
    },
});

export const fetchWeather = (city, country) => (dispatch, getState) => {
  fetch(`http://api.openweathermap.org/data/2.5/forecast?q=${city},${country}&appid=80871cbe062138d667d1fccf1d4d9eaa&units=metric`)
    .then(data => data.json())
    .then(data => dispatch(getWeatherAction(data)))
    .then(data => dispatch(currentDayAction(getState().weatherReducer.today)));
}