let price = 100,
saucePrice = 0,
toppingPrice = 0,
ingredients = []

// drag and drop

const table = document.querySelector('.table')
const sauceClassic = document.querySelector('#sauceClassic')
const sauceBBQ = document.querySelector('#sauceBBQ')
const sauceRikotta = document.querySelector('#sauceRikotta')
const moc1 = document.querySelector('#moc1')
const moc2 = document.querySelector('#moc2')
const moc3 = document.querySelector('#moc3')
const telya = document.querySelector('#telya')
const vetch1 = document.querySelector('#vetch1')
const vetch2 = document.querySelector('#vetch2')
const draggable = document.querySelectorAll('.draggable')

table.addEventListener('drop', drop)
table.addEventListener('dragover', allowDrop)
table.addEventListener('dragenter', dragEnter)
table.addEventListener('dragleave', dragLeave)

for(let draggingElement of draggable) {
    draggingElement.addEventListener('dragstart', drag)
}

function allowDrop(ev) {
    ev.preventDefault()
}

function dragEnter(ev) {
    this.className += ' hovered'
}

function dragLeave(ev) {
    this.className = 'table'
}

function drag(ev) {
    ev.dataTransfer.setData("text", ev.target.id)
}

function drop(ev) {
    ev.preventDefault()
    let data = ev.dataTransfer.getData("text")

    if(!(table.contains(document.querySelector(`#${data}1`)))) {
        let nodeCopy = document.getElementById(data).cloneNode(true)
        nodeCopy.id = `${data}1`

        this.append(nodeCopy)
        
        nodeCopy.setAttribute('draggable', 'false')
        nodeCopy.removeAttribute('class')
        nodeCopy.removeEventListener('dragstart', drag)
    }
    
    this.className = 'table'

    checkIng()
}

// размер пиццы

const radio = document.querySelector('#pizza')
const radio1 = document.querySelector('#small')
const radio2 = document.querySelector('#mid')
const radio3 = document.querySelector('#big')
const parag = document.querySelectorAll('p')

radio.onchange = function() {
    if(radio1.checked){
        table.style.width = '40%'
        price = 50
    }
    if(radio2.checked){
        table.style.width = '45%'
        price = 75
    }
    if(radio3.checked){
        table.style.width = '50%'
        price = 100
    }
    parag[0].innerHTML = `Цiна: ${price + saucePrice + toppingPrice} грн`
}

const result = document.querySelector('.result')
const sauces = document.querySelector('.sauces')
const topings = document.querySelector('.topings')


// добавление по клику

function click() {
    if(!(table.contains(document.querySelector(`#${this.getAttribute('id')}1`)))) {
        let nodeCopy = document.getElementById(this.getAttribute('id')).cloneNode(true)
        nodeCopy.id = `${this.getAttribute('id')}1`
        table.append(nodeCopy)
    }
    checkIng()
}

sauceClassic.addEventListener('click', click)
sauceBBQ.addEventListener('click', click)
sauceRikotta.addEventListener('click', click)
moc1.addEventListener('click', click)
moc2.addEventListener('click', click)
moc3.addEventListener('click', click)
telya.addEventListener('click', click)
vetch1.addEventListener('click', click)
vetch2.addEventListener('click', click)


// добавление и удаление выбранного соуса в список

function element(text) {
    let el = document.createElement('p')
    el.innerHTML = text
    el.classList = 'element'
    
    let x = document.createElement('button')
    x.innerHTML = 'X'
    x.classList = 'x'
    x.onclick = function() {
        if(text === ' Кетчуп') {
            const sauceClassic1 = document.querySelector('#sauceClassic1')   
            sauceClassic1.remove()
            saucePrice -= 10
            parag[0].innerHTML = `Цiна: ${price + saucePrice + toppingPrice} грн`
        }
        if(text === ' BBQ') {    
            const sauceBBQ1 = document.querySelector('#sauceBBQ1')       
            sauceBBQ1.remove()
            saucePrice -= 10
            parag[0].innerHTML = `Цiна: ${price + saucePrice + toppingPrice} грн`
        }
        if(text === ' Рiкотта') {
            const sauceRikotta1 = document.querySelector('#sauceRikotta1')   
            sauceRikotta1.remove()
            saucePrice -= 10
            parag[0].innerHTML = `Цiна: ${price + saucePrice + toppingPrice} грн`
        }
        x.parentElement.remove()

    }

    let element = document.createElement('div')
    element.classList = 'div'
    element.append(el)
    element.append(x)

    return element
}

// добавление и удаление выбранного топпинга в список

function element2(text) {
    let el = document.createElement('p')
    el.innerHTML = text
    el.classList = 'element'
    
    let x = document.createElement('button')
    x.innerHTML = 'X'
    x.classList = 'x'
    x.onclick = function() {
        if(text === ' Сир звичайний') {
            const moc11 = document.querySelector('#moc11')   
            moc11.remove()
            toppingPrice -= 10
            parag[0].innerHTML = `Цiна: ${price + saucePrice + toppingPrice} грн`
        }
        if(text === ' Сир фета') {    
            const moc21 = document.querySelector('#moc21')       
            moc21.remove()
            toppingPrice -= 10
            parag[0].innerHTML = `Цiна: ${price + saucePrice + toppingPrice} грн`
        }
        if(text === ' Моцарелла') {
            const moc31 = document.querySelector('#moc31')   
            moc31.remove()
            toppingPrice -= 10
            parag[0].innerHTML = `Цiна: ${price + saucePrice + toppingPrice} грн`
        }
        if(text === ' Телятина') {
            const telya1 = document.querySelector('#telya1')   
            telya1.remove()
            toppingPrice -= 10
            parag[0].innerHTML = `Цiна: ${price + saucePrice + toppingPrice} грн`
        }
        if(text === ' Помiдори') {    
            const vetch11 = document.querySelector('#vetch11')       
            vetch11.remove()
            toppingPrice -= 10
            parag[0].innerHTML = `Цiна: ${price + saucePrice + toppingPrice} грн`
        }
        if(text === ' Гриби') {
            const vetch21 = document.querySelector('#vetch21')   
            vetch21.remove()
            toppingPrice -= 10
            parag[0].innerHTML = `Цiна: ${price + saucePrice + toppingPrice} грн`
        }
        x.parentElement.remove()

    }

    let element = document.createElement('div')
    element.classList = 'div'
    element.append(el)
    element.append(x)

    return element
}

function checkIng() {

    // Соуси

    function check(inner) {
        let elements = document.querySelectorAll('.element')
        let isTrue = true
        if(elements.length > 0) elements.forEach(element => element.innerHTML == inner ? isTrue = false : console.log()) 
        return isTrue
    }

    if(table.contains(document.querySelector('#sauceClassic1')) && check(' Кетчуп')) {
        sauces.append(element(' Кетчуп'))

        saucePrice = (sauces.childNodes.length - 1) * 10
        parag[0].innerHTML = `Цiна: ${price + saucePrice + toppingPrice} грн`
    }

    if(table.contains(document.querySelector('#sauceBBQ1')) && check(' BBQ')) {
        sauces.append(element(' BBQ'))

        saucePrice = (sauces.childNodes.length - 1) * 10
        parag[0].innerHTML = `Цiна: ${price + saucePrice + toppingPrice} грн`
    }

    if(table.contains(document.querySelector('#sauceRikotta1')) && check(' Рiкотта')) {
        sauces.append(element(' Рiкотта'))

        saucePrice = (sauces.childNodes.length - 1) * 10
        parag[0].innerHTML = `Цiна: ${price + saucePrice + toppingPrice} грн`
    }

    // Топинги

    if(table.contains(document.querySelector('#moc11')) && check(' Сир звичайний')) {
        topings.append(element2(' Сир звичайний'))

        toppingPrice = (topings.childNodes.length - 1) * 10
        parag[0].innerHTML = `Цiна: ${price + saucePrice + toppingPrice} грн`
    }

    if(table.contains(document.querySelector('#moc21')) && check(' Сир фета')) {
        topings.append(element2(' Сир фета'))

        toppingPrice = (topings.childNodes.length - 1) * 10
        parag[0].innerHTML = `Цiна: ${price + saucePrice + toppingPrice} грн`
    }

    if(table.contains(document.querySelector('#moc31')) && check(' Моцарелла')) {
        topings.append(element2(' Моцарелла'))

        toppingPrice = (topings.childNodes.length - 1) * 10
        parag[0].innerHTML = `Цiна: ${price + saucePrice + toppingPrice} грн`
    }

    if(table.contains(document.querySelector('#telya1')) && check(' Телятина')) {
        topings.append(element2(' Телятина'))

        toppingPrice = (topings.childNodes.length - 1) * 10
        parag[0].innerHTML = `Цiна: ${price + saucePrice + toppingPrice} грн`
    }

    if(table.contains(document.querySelector('#vetch11')) && check(' Помiдори')) {
        topings.append(element2(' Помiдори'))

        toppingPrice = (topings.childNodes.length - 1) * 10
        parag[0].innerHTML = `Цiна: ${price + saucePrice + toppingPrice} грн`
    }

    if(table.contains(document.querySelector('#vetch21')) && check(' Гриби')) {
        topings.append(element2(' Гриби'))

        toppingPrice = (topings.childNodes.length - 1) * 10
        parag[0].innerHTML = `Цiна: ${price + saucePrice + toppingPrice} грн`
    }
}

// Скидка 30%

const banner = document.querySelector('#banner')

banner.addEventListener('mousemove', function() {
    banner.style.right = `${Math.random() * 100 - 10}%`
    banner.style.bottom = `${Math.random() * 100 - 10}%`
})

// Валидация формы

function error(text, element) {
    let er = document.createElement('p')
    er.innerText = text
    document.querySelector('.grid').insertBefore(er, element)
}



document.info.btnSubmit.addEventListener('click', function validation() {
    const name = document.info.name.value.trim()
    const phone = document.info.phone.value.trim()
    const email = document.info.email.value.trim()
    const labels = document.querySelectorAll('label')

    if(name.length > 1 && !(name.match(/[^а-я А-Я a-z A-Z]/))) {
        if(document.info.name.classList == 'error') {
            document.info.querySelectorAll('p')[0].remove()
        }
        document.info.name.classList = 'success'
    } else if(name.length > 1) {
        if(document.info.name.classList == 'error') {
            document.info.querySelectorAll('p')[0].remove()
        }
        document.info.name.classList = 'error'
        error(`Iм'я не повинне складатися з цифр`, labels[4])
    } else {
        if(document.info.name.classList == 'error') {
            document.info.querySelectorAll('p')[0].remove()
        }
        document.info.name.classList = 'error'
        error('Мiнiмум 2 букви', labels[4])
    }

    if(phone.length > 4 && !(phone.match(/[^0-9-+ ]/))) {
        if(document.info.phone.classList == 'error') {
            if(document.info.querySelectorAll('p')[1]) {
                document.info.querySelectorAll('p')[1].remove()
            } else if (document.info.querySelectorAll('p')[0]) {
                document.info.querySelectorAll('p')[0].remove()
            } 
        }
        document.info.phone.classList = 'success'
    } else if(phone.length > 4) {
        if(document.info.phone.classList == 'error') {
            if(document.info.querySelectorAll('p')[1]) {
                document.info.querySelectorAll('p')[1].remove()
            } else if (document.info.querySelectorAll('p')[0]) {
                document.info.querySelectorAll('p')[0].remove()
            } 
        }
        document.info.phone.classList = 'error'
        error('Лише цифри та знаки "+" або "-"', labels[5])
    } else {        
        if(document.info.phone.classList == 'error') {
            if(document.info.querySelectorAll('p')[1]) {
                document.info.querySelectorAll('p')[1].remove()
            } else if (document.info.querySelectorAll('p')[0]) {
                document.info.querySelectorAll('p')[0].remove()
        } 
    }
        document.info.phone.classList = 'error'
        error('Мiнiмум 5 цифр', labels[5])
    }

    if(email.length > 4 && email.match(/^[^\s@]+@[^\s@]+\.[^\s@]/) && !(email.match(/(\..*){2,}/)) && !(email.split('').filter(l => l == '@').length > 1)) {
        if(document.info.email.classList == 'error') {
            if(document.info.querySelectorAll('p')[2]) {
                document.info.querySelectorAll('p')[2].remove()
            } else if (document.info.querySelectorAll('p')[1]) {
                document.info.querySelectorAll('p')[1].remove()
            } else if (document.info.querySelectorAll('p')[0]) {
                document.info.querySelectorAll('p')[0].remove()
            }
        }
        document.info.email.classList = 'success'
    } else if(document.info.email.classList != 'error') {
        document.info.email.classList = 'error'
        error('Введiть дiйсний email', document.info.cancel)
    }

    if(document.info.name.classList == 'success' && document.info.phone.classList == 'success' && document.info.email.classList == 'success') {
        const inputTopping = document.createElement('input')
        inputTopping.setAttribute('name', 'toppings')
        inputTopping.setAttribute('type', 'hidden')
        document.info.append(inputTopping)

        const tList = []
        if(table.contains(document.querySelector('#moc11'))) tList.push('сир звичайний')
        if(table.contains(document.querySelector('#moc21'))) tList.push('сир фета')
        if(table.contains(document.querySelector('#moc31'))) tList.push('моцарелла')
        if(table.contains(document.querySelector('#telya1'))) tList.push('телятина')
        if(table.contains(document.querySelector('#vetch11'))) tList.push('помiдори')
        if(table.contains(document.querySelector('#vetch21'))) tList.push('гриби')

        inputTopping.value = tList.join(', ')

        const inputSauses = document.createElement('input')
        inputSauses.setAttribute('name', 'sauses')
        inputSauses.setAttribute('type', 'hidden')
        document.info.append(inputSauses)

        const sList = []
        if(table.contains(document.querySelector('#sauceClassic1'))) sList.push('кетчуп')
        if(table.contains(document.querySelector('#sauceBBQ1'))) sList.push('BBQ')
        if(table.contains(document.querySelector('#sauceRikotta1'))) sList.push('рiкотта')

        inputSauses.value = sList.join(', ')

        const inputSize = document.createElement('input')
        inputSize.setAttribute('name', 'size')
        inputSize.setAttribute('type', 'hidden')
        document.info.append(inputSize)
        if(radio1.checked) inputSize.value = 'маленька'
        if(radio2.checked) inputSize.value = 'середня'
        if(radio3.checked) inputSize.value = 'велика'

        const inputPrice = document.createElement('input')
        inputPrice.setAttribute('name', 'price')
        inputPrice.setAttribute('type', 'hidden')
        document.info.append(inputPrice)

        inputPrice.value = price + saucePrice + toppingPrice

        document.info.submit()
    }
})


