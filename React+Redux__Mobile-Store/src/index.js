import React from 'react';
import ReactDOM from 'react-dom';
import './css/index.css';
import App from './App';
import { Provider } from 'react-redux';
import { createStore } from 'redux';
import allReducers from './Reducers';
import 'bootstrap/dist/css/bootstrap.min.css';

export const store = createStore(allReducers, window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__())

const RootApp = () => (
  <Provider store={store}>
      <App />
  </Provider>
);

ReactDOM.render(<RootApp />, document.getElementById('root'));
