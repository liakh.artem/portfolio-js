const cartItems = (state = [], action) => {
  switch (action.type) {
    case 'ADD_ITEM':
      const { phone } = action.payload,
        newPhone = Object.assign({}, phone);
      if(!state.some(item => item.id === phone.id)) {
        state = [ ...state, newPhone ];
      } else {
        const thisPhone = state.filter(item => item.id === phone.id)[0];
        thisPhone.number++;
      }
      return state;
    case 'REMOVE_ONE':
      const { phoneToRemoveOne } = action.payload,      
        thisPhone = state.filter(item => item.id === phoneToRemoveOne.id)[0];
      if(thisPhone.number > 1) thisPhone.number--;
      else return state.filter(phone => phone.id !== phoneToRemoveOne.id);
      return state;
    case 'REMOVE_ITEM':
      const { phoneToRemove } = action.payload,
        newState = state.filter(phone => phone.id !== phoneToRemove.id);
      return newState;
    default:
      return state;
  }
}

export default cartItems;