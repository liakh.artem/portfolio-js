import { combineReducers } from 'redux';
import allPhonesList from './allPhonesList';
import cartItems from './cartItems';
import searchReducer from './search';
import { reducer as formReducer } from 'redux-form';

export default combineReducers({
    allPhonesList,
    cartItems,
    searchReducer,
    form: formReducer,
});