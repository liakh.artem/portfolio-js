import initialState from '../data/initialState';

const searchReducer = (state = {}, action) => {
    switch (action.type) {
        case 'SEARCH':
            const { search } = action.payload,
                lowSearch = search.toLowerCase();
            state = initialState.filter(phone => phone.name.toLowerCase().includes(lowSearch));
        return state;
        default:
        return state;
    }
}

export default searchReducer;