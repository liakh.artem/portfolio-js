const removeOneItemAction = (phoneToRemoveOne) => ({
    type: 'REMOVE_ONE',
    payload: {
        phoneToRemoveOne,
    },
});

export default removeOneItemAction;