const addToCartAction = (phone) => ({
    type: 'ADD_ITEM',
    payload: {
        phone,
    },
});

export default addToCartAction;