const removeFromCartAction = (phoneToRemove) => ({
    type: 'REMOVE_ITEM',
    payload: {
        phoneToRemove,
    },
});

export default removeFromCartAction;
