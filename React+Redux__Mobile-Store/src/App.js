import React, { Fragment } from 'react';
import Navbar from './Components/Navbar';
import Cart from './Components/Cart';
import Header from './Components/Header';
import SearchResults from './Components/SearchResults';
import PhoneList from './Components/PhoneList';
import PhoneInfo from './Components/PhoneInfo';
import ContactUs from './Components/ContactUs';
import Footer from './Components/Footer';
import {
    BrowserRouter as Router,
    Switch,
    Route,
} from "react-router-dom";

function App() {
    return (
        <Fragment>
            <Router>
                <Navbar />
                <div className="container main-area">
                <Switch>
                    <Route path="/" exact  render={props =>
                        <Fragment>
                            <Header />
                            <PhoneList />
                            <ContactUs />
                        </Fragment>
                    } />
                    <Route path="/phone/:id" component={PhoneInfo} />
                    <Route path="/search/:value" exact  component={SearchResults} />
                    <Route path="/cart" exact  component={Cart} />
                </Switch>
                </div>
                <Footer />
            </Router>
        </Fragment>
    )
}

export default App;
