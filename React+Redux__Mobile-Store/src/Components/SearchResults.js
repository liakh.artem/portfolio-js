import React, { Fragment } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import { withRouter } from 'react-router-dom';
import initialState from '../data/initialState';
import cn from 'classnames';
import addToCartAction from '../Actions/addToCartAction';
import removeFromCartAction from '../Actions/removeFromCartAction';

const mapStateToProps = (state) => {
    const props = state;
    return props;
};

function SearchResults(props) {
    const name = props.match.params.value.toLowerCase(),
        value = initialState.filter(phone => phone.name.toLowerCase().includes(name)),
        { allPhonesList, cartItems } = props,
            addToCard = (phone) => (e) => {
                e.preventDefault();
                if(!cartItems.some(item => item.id === phone.id)) {
                    props.dispatch(addToCartAction(phone))
                } else {
                    props.dispatch(removeFromCartAction(phone))            
                }
            },
            buttonClasses = (phone) => {
                return cn({
                    'btn': true,
                    'btn-success': !cartItems.some(item => item.id === phone.id),
                    'btn-danger': cartItems.some(item => item.id === phone.id),
                    'm-1': true,
                });
            };
    return(
        <Fragment>
        <div className="row justify-content-center m-4">
            <h1>Search Results</h1>
        </div>
        <div className="row">
        {value.map(phone => (
            <div key={phone.id} className="card col-3 text-center" data-id={phone.id}>
                <div className="d-flex flex-wrap align-items-center"><img className="card-img-top w-50 mx-auto p-2" src={`/img/phones/${phone.id}.jpg`} alt={phone.name}/></div>
                <div className="card-body">
                    <h4 className="card-title">{phone.name}</h4>
                    <p className="card-text">{phone.price}$</p>
                    <a onClick={addToCard(phone)} href="#" className={buttonClasses(phone)}>{!cartItems.some(item => item.id === phone.id) ? 'Buy now!' : 'Remove'}</a>
                    <Link to={`/phone/${phone.id}`} className="btn btn-outline-primary m-1">Read more</Link>
                </div>
            </div>
        ))}
        </div>
        </Fragment>
    );
}

const SearchWithRouter = withRouter(SearchResults);
export default connect(mapStateToProps)(SearchWithRouter);