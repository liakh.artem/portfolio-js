import React from 'react';
import { connect } from 'react-redux';
import { withRouter, Link } from 'react-router-dom';
import cn from 'classnames';
import addToCartAction from '../Actions/addToCartAction';
import removeFromCartAction from '../Actions/removeFromCartAction';
import initialState from '../data/initialState';

const mapStateToProps = (state) => {
    const props = state;
    return props;
};

function PhoneInfo(props) {
    const phone = initialState.filter(phone => phone.id == props.match.params.id)[0],
        { allPhonesList, cartItems } = props,
        addToCard = (phone) => (e) => {
            e.preventDefault();
            if(!cartItems.some(item => item.id === phone.id)) {
                props.dispatch(addToCartAction(phone))
            } else {
                props.dispatch(removeFromCartAction(phone))            
            }
        },
        buttonClasses = (phone) => {
            console.log(!cartItems.some(item => item.id === phone.id));
            return cn({
                'btn': true,
                'btn-success': !cartItems.some(item => item.id === phone.id),
                'btn-danger': cartItems.some(item => item.id === phone.id),
                'm-1': true,
            });
        };
    return(
        <div className="container">
            <div className="row">
                <div className="col-4">
                    <img className="w-100 p-5" src={`/img/phones/${phone.id}.jpg`}/>
                </div>
                <div className="col-8 d-flex flex-column align-items-center pt-5">
                    <Link className="mr-auto pb-3" to="/">{"<< Back to Home page"}</Link>
                    <h2>{phone.name}</h2>
                    <p>Price: {phone.price}$</p>
                    <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. 
                        Aenean commodo ligula eget dolor. Aenean massa. 
                        Cum sociis natoque penatibus et magnis dis parturient montes, 
                        nascetur ridiculus mus. Donec quam felis, ultricies nec,
                         pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. 
                         Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. 
                         In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. 
                         Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. 
                         Vivamus elementum semper nisi. Aenean vulputate eleifend tellus. </p>                         
                    <a onClick={addToCard(phone)} href="#" className={buttonClasses(phone)}>{!cartItems.some(item => item.id === phone.id) ? 'Buy now!' : 'Remove'}</a>
                </div>
            </div>
        </div>
    )
}

const InfoWithRouter = withRouter(PhoneInfo);
export default connect(mapStateToProps)(InfoWithRouter);