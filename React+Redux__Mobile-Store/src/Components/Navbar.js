import Search from './Search';
import { Link } from 'react-router-dom';
import React from 'react';
import { connect } from 'react-redux';

const mapStateToProps = ({ cartItems }) => {
    const props = { cartItems };
    return props;
};

function Navbar(props) {
    const moveToContact = () => {
        const contact = document.querySelector('.contact');
        contact.scrollIntoView({behavior: "smooth"});
    }
    return(
        <div className="navbar bg-white pt-4 pb-4">
            <div className="col-3 col-xl-2">
                <span className="navbar-brand ml-5"><b>PHONE</b>STORE</span>
            </div>
            <div className="col-3">
                <nav className="navbar navbar-expand-lg navbar-light">
                    <ul className="navbar-nav">
                        <li className="nav-item mr-3"><Link to="/">Home</Link></li>
                        <li onClick={moveToContact} className="contact-link nav-item mr-3">Contact Us</li>
                    </ul>
                </nav>
            </div>
            <div className="col-2 col-xl-3">
                <Search />
            </div>
            <div className="col-4 col-xl-3 text-right">
                <Link to="/cart"><img src="/img/cart.png" className="icon p-1"/></Link>
                <span class="cart-number">{props.cartItems.length}</span>
                <img src="/img/phone.png" className="icon p-1"/><span className="p-1">800-123-456</span>
                <img src="/img/profile.png" className="icon  p-1 mr-5"/>
            </div>
        </div>
    );
}

export default connect(mapStateToProps)(Navbar);