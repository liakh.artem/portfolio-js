import React from 'react';

function Header() {
    return(
        <div className="row align-items-center bg-primary mt-4">
            <div className="col">
                <img className="w-100 ml-5" src="/img/header-phones.png" alt="header"/>
                </div>
            <div className="col text-center">
                <h1 className="text-light">PHONE STORE</h1>
                <p className="text-light">On React with Redux</p>
            </div>
        </div>
    )
}

export default Header;