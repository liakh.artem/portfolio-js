import { Field, reduxForm } from 'redux-form';
import React from 'react';
import { store } from '../index';
import { Link } from 'react-router-dom';
import { withRouter } from 'react-router-dom';

let url = '/search';

const getResults = (values) => {
    url = `/search/${values.search}`
};

const Search = (props) => {
    const handleSubmit = (e) => {
        props.handleSubmit(e);
        props.history.push(url);
    }
    return(
        <form onSubmit={handleSubmit} className="search row">
            <div className="col-10 mt-1 p-0">
                <Field className="form-control p-0 pl-3" component="input" required name="search" placeholder="..."/>
            </div>
            <div className="col-2 p-0">
                <button className="btn" type="submit"><img  className="icon" src="/img/search.png" alt="search icon" /></button>
            </div>            
        </form>
    )
};

const SearchWithRouter = withRouter(Search);
export default reduxForm({
    form: 'search',
    onSubmit: getResults,
  })(SearchWithRouter);