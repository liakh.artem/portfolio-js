import React from 'react';

function ContactUs() {
    return(
        <form class="contact">
            <h3 className="text-center m-4">Contact Us</h3>
            <div className="form-row m-3">
                <div className="col">
                    <input type="text" className="form-control" placeholder="First name" />
                </div>
                <div className="col">
                    <input type="text" className="form-control" placeholder="Last name" />
                </div>
            </div>
            <div className="form-row m-3">
            <div className="col text-center">
            <textarea className="form-control" id="exampleFormControlTextarea1" rows="3" placeholder="Your message..."></textarea>
            <input className="btn btn-primary mt-3 w-100" value="Send" type="button"/>
            </div>
            </div>
        </form>
    )
}

export default ContactUs;