import React, { Fragment } from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import useForceUpdate from 'use-force-update';
import addToCartAction from '../Actions/addToCartAction';
import removeFromCartAction from '../Actions/removeFromCartAction';
import removeOneItemAction from '../Actions/removeOneItemAction';

const mapStateToProps = ({ cartItems }) => {
    const props = { cartItems };
    return props;
};

function Cart(props) {
    const { cartItems } = props,
        totalCartPrice = () => cartItems.reduce((sum, item) => sum + (item.price * (item.number)), 0),
        forceUpdate = useForceUpdate(),
        addOneMore = (phone) => {
            props.dispatch(addToCartAction(phone))
            forceUpdate();
        },
        removeOne = (phone) => {
            props.dispatch(removeOneItemAction(phone));
            forceUpdate();
        }
    return(
        cartItems.length === 0 ? 
        <div className="text-center">
            <h1 className="m-5">Cart</h1>
            <h2 className="mt-5 mb-4">Your cart is empty</h2>            
            <Link to="/"><button className="btn btn-success">{'Go shopping >>'}</button></Link>
        </div> :
        <Fragment>
        {cartItems.map(phone => (
            <div key={phone.id} className="row d-flex align-items-center bg-white border">
                <div className="col">
                    <img className="card-img-top w-50 mx-auto p-2" src={`/img/phones/${phone.id}.jpg`} alt={phone.name}/>
                </div>
                <div className="col">
                    <p className="mb-0">{phone.name}</p>
                </div>
                <div className="col">
                    <button onClick={() => removeOne(phone)} className="btn btn-danger">-</button>
                    <b className="mr-2 ml-2">Pcs: {phone.number}</b>
                    <button onClick={() => addOneMore(phone)} className="btn btn-success">+</button>
                </div>
                <div className="col-3">
                    <p>Price: {phone.price}$</p>
                    <p>Total: {phone.price * phone.number}$</p>
                </div>
                <div className="col">
                    <button onClick={() => props.dispatch(removeFromCartAction(phone))} className="btn btn-danger">Remove</button>
                </div>
            </div>
        ))}
        <div className="row d-flex align-items-center bg-dark text-white border">
            <div className="col-7 pt-4 pb-3"><h5>Total price:</h5></div>
            <div className="col-5 pt-4 pb-3"><h5>{totalCartPrice()}$</h5></div>
        </div>
        </Fragment>
    );
}

export default connect(mapStateToProps)(Cart);