import React from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import cn from 'classnames';
import addToCartAction from '../Actions/addToCartAction';
import removeFromCartAction from '../Actions/removeFromCartAction';

const mapStateToProps = ({ allPhonesList, cartItems }) => {
    const props = { allPhonesList, cartItems };
    return props;
};

function PhoneList(props) {
    const { allPhonesList, cartItems } = props,
        addToCard = (phone) => (e) => {
            e.preventDefault();
            if(!cartItems.some(item => item.id === phone.id)) {
                props.dispatch(addToCartAction(phone))
            } else {
                props.dispatch(removeFromCartAction(phone))            
            }
        },
        buttonClasses = (phone) => {
            return cn({
                'btn': true,
                'btn-success': !cartItems.some(item => item.id === phone.id),
                'btn-danger': cartItems.some(item => item.id === phone.id),
                'm-1': true,
            });
        };
    return(
        <div className="row mt-4">
        {allPhonesList.map(phone => (
            <div key={phone.id} className="card col-3 text-center" data-id={phone.id}>
                <div className="d-flex flex-wrap align-items-center"><img className="card-img-top w-50 mx-auto p-2" src={`/img/phones/${phone.id}.jpg`} alt={phone.name}/></div>
                <div className="card-body">
                <h4 className="card-title">{phone.name}</h4>
                <p className="card-text">{phone.price}$</p>
                    <a onClick={addToCard(phone)} href="#" className={buttonClasses(phone)}>{!cartItems.some(item => item.id === phone.id) ? 'Buy now!' : 'Remove'}</a>
                    <Link to={`/phone/${phone.id}`} className="btn btn-outline-primary m-1">Read more</Link>
                </div>
            </div>
        ))}
        </div>
    );
}

export default connect(mapStateToProps)(PhoneList);