const initialState = [
    { id: 1, price: 700, name: 'Samsung Galaxy M21', number: 1},
    { id: 2, price: 910, name: 'Apple iPhone SE', number: 1},
    { id: 3, price: 290, name: 'Samsung Galaxy M11', number: 1},
    { id: 4, price: 820, name: 'Samsung Galaxy S10', number: 1},
    { id: 5, price: 730, name: 'Huawei P40 lite', number: 1},
    { id: 6, price: 360, name: 'Nokia 7.2', number: 1},
    { id: 7, price: 230, name: 'Huawei P30 Lite', number: 1},
    { id: 8, price: 540, name: 'ZTE Blade 20 Smart', number: 1},
    { id: 9, price: 550, name: 'Motorola RAZR', number: 1},
    { id: 10, price: 640, name: 'Xiaomi Redmi Note 8 Pro', number: 1},
    { id: 11, price: 280, name: 'OPPO A31', number: 1},
    { id: 12, price: 1140, name: 'Apple iPhone 11', number: 1},
];

export default initialState;