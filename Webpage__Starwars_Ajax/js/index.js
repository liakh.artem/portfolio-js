const xhr = new XMLHttpRequest(), // Создаю новый объект XMLHttpRequest
starsDiv = document.querySelector('.stars') // основная часть страницы

function getStars(link, ifNew) { // Функция для отображение персонажей

    // если страница уже была создана, удаляю старую информацию
    if(ifNew) {
        starsDiv.innerHTML = ''
        loader.classList = ''
    }

    xhr.open("GET", link) // Конфигурирую его: GET-запрос на URL 'https://swapi.co/api/people/...'
    
    xhr.addEventListener('load', function () { // По получению выполняю функцию
        if (xhr.status == 200) { // Если успешно получено
            
            // показываю переключение страниц внизу
            page.classList = ''
            prev.classList = ''
            next.classList = ''

            // Скрываю loader
            loader.classList = 'hide' 
            const data = JSON.parse(xhr.responseText), // Перевожу JSON строку в объект
            
            // Получаю имя, пол, родной мир персонажа и ссылку на JSON строку с кораблями
            name = data.results.map(element => element.name),
            homeworld = data.results.map(element => element.homeworld),
            gender = data.results.map(element => element.gender),
            starship = data.results.map(element => element.starships)

            // Пробегаю по всем персонажам и передаю информацию о них на страницу
            for(i = 0; i < data.results.length; i++) {                
                function planetsList(element) {

                    // Переношу переменные в область видимости
                    const b = i
                    const div = element

                    // Узнаю планету персонажа
                    const planetsXhr = new XMLHttpRequest(); // Создаю новый объект XMLHttpRequest для получения планеты
                    planetsXhr.open("GET", homeworld[i]) // Конфигурирую его: GET-запрос на URL 'https://swapi.co/api/people/'

                    // Создаю loader
                    const loader3 = document.createElement('img')
                    loader3.setAttribute('src', './images/loader.gif')
                    loader3.setAttribute('alt', 'loader')
                    loader3.id = 'loader3'
                    loader3.classList = ''
                    div.append(loader3)
                    planetsXhr.addEventListener('load', function () { // По получению выполняю функцию
                        if (planetsXhr.status == 200) { // Если успешно получено                            
                            loader3.classList = 'hide' // Скрываю loader
                            const planets = JSON.parse(planetsXhr.responseText) // Перевожу JSON строку в объект
                            
                            // Перевожу на русский и добавляю информацию на страницу
                            new Promise(resolve => {
                                const starInfo = document.createElement('p')
                                let genderRu = gender[b]
                                if(gender[b] === 'male') genderRu = 'мужской'
                                if(gender[b] === 'female') genderRu = 'женский'
                                starInfo.innerHTML += `Пол: ${genderRu}<br>`                           
                                starInfo.innerHTML += `Родная планета: ${planets.name}<br>` 
                                div.append(starInfo)
                                resolve()
                            }).then(() => starshipsList(div, b)) // создаю промис, чтобы был правильный порядок информации
                        } else loader3.setAttribute('src', './images/error.png')  // показываю знак ошибки
                    })
                    planetsXhr.send() // Отсылаю запрос
                }

                // Добавляю имя на страницу
                const starName = document.createElement('h2')
                starName.innerHTML = name[i]

                // Узнаю корабли персонажа
                function starshipsList(el1, el2) {

                    // Переношу переменные в область видимости
                    const div = el1
                    const c = el2

                    // Если персонаж управлял каким-то кораблем, создаю кнопку Список кораблей                                       
                    if(starship[c].length > 0) {
                        const button = document.createElement('button')
                        button.innerText = 'Список кораблей'
                        const stShip = document.createElement('p')
                        stShip.classList = 'hide'
                        const drivingShips = document.createElement('h3')
                        drivingShips.innerText = 'Пилотируемые корабли'
                        drivingShips.classList = 'hide'

                        // Создаю loader
                        const loader2 = document.createElement('img')
                        loader2.setAttribute('src', './images/loader.gif')
                        loader2.setAttribute('alt', 'loader')
                        loader2.id = 'loader2'
                        loader2.classList = 'hide'

                        // Перебираю ссылки на все корабли персонажа
                        starship[c].forEach(each => {    
                            const starshipXhr = new XMLHttpRequest(); // Создаю новый объект XMLHttpRequest для получения кораблей
                            starshipXhr.open("GET", each) // Конфигурирую его: GET-запрос на URL 'https://swapi.co/api/people/'
                            starshipXhr.addEventListener('load', function () { // По получению выполняю функцию
                                if (starshipXhr.status == 200) { // Если успешно получено
                                    loader2.classList = 'hide' // Скрываю loader
                                    const starshipData = JSON.parse(starshipXhr.responseText), // Перевожу JSON строку в объект   

                                    // Получаю название, модель, класс, к-во пассажиров и производителя корабля
                                    starshipName = starshipData.name,                                 
                                    model = starshipData.model,                                 
                                    starship_class = starshipData.starship_class,                                 
                                    passengers = starshipData.passengers,                                 
                                    manufacturer = starshipData.manufacturer
                                    
                                    // Добавляю информацию на страницу                                    
                                    const a = document.createElement('a') // Делать каждое имя корабля ссылкой
                                    a.setAttribute('href', '#/')                               
                                    a.innerHTML += `${starshipData.name}`
                                    stShip.append(a)

                                    // Подробные характеристики корабля
                                    const shipInfo = document.createElement('p')
                                    shipInfo.innerHTML = `Модель: ${model}<br>Класс: ${starship_class}<br>Пассажировместимость: ${passengers}<br>Производство: ${manufacturer}<br>`
                                    shipInfo.classList = 'hide'
                                    stShip.append(shipInfo)

                                    // При клике появляется подробные характеристики корабля
                                    a.addEventListener('click', function(ev) { 
                                        if(shipInfo.classList == 'hide') shipInfo.classList = ''
                                        else shipInfo.classList = 'hide'
                                    })
                                } else loader2.setAttribute('src', './images/error.png')  // показываю знак ошибки
                            })
                            starshipXhr.send() // Отсылаю запрос
                        })

                        // Добавляю элементы на страницу
                        div.append(button)
                        div.append(drivingShips)
                        div.append(loader2)
                        div.append(stShip)

                        // При клике на кнопку вывожу корабли, заменив кнопку Список кораблей на тег h3 с текстом Пилотируемые корабли 
                        button.addEventListener('click', function() {
                            if(stShip.childNodes.length === 0) loader2.classList = ''
                            stShip.classList = ''
                            drivingShips.classList = ''
                            button.classList = 'hide'
                            
                            // Если имя корабля - Тысячелетний сокол (Millennium Falcon), 
                            // то под списком кораблей вывожу крупными буквами надпись: 
                            // Хан Соло стрелял первым!
                            if(Array.from(this.parentElement.lastChild.childNodes).some(child => child.innerText === 'Millennium Falcon')){
                                const han = document.createElement('h4')
                                han.innerHTML = `Хан Соло стрелял первым!`
                                div.append(han)
                            }
                        })
                    }
                }             
                
                const div = document.createElement('div') // создаю ячейку

                 // Добавление фото некоторым персонажам и силуэт по умолчанию
                function setPhoto(name) {
                    const photo = document.createElement('img')
                    photo.setAttribute('src', `./images/${name}.png`)
                    if(name === 'silhouette') photo.classList = 'no'                   
                    div.append(photo)
                }                
                if(name[i] === 'Luke Skywalker') setPhoto('Luke')
                else if(name[i] === 'C-3PO') setPhoto('C-3PO')
                else if(name[i] === 'R2-D2') setPhoto('R2-D2')
                else if(name[i] === 'Darth Vader') setPhoto('DarthVader')
                else if(name[i] === 'Leia Organa') setPhoto('Leia')
                else if(name[i] === 'Owen Lars') setPhoto('Owen')
                else if(name[i] === 'Beru Whitesun lars') setPhoto('Beru')
                else if(name[i] === 'R5-D4') setPhoto('R5-D4')
                else if(name[i] === 'Chewbacca') setPhoto('Chewbacca')
                else if(name[i] === 'Obi-Wan Kenobi') setPhoto('Obi-Wan')
                else if(name[i] === 'Han Solo') setPhoto('Han')                
                else setPhoto('silhouette') // силуэт для остальных

                // Добавляю всю информацию 
                div.append(starName)
                planetsList(div)

                if(!ifNew) starsDiv.append(div) // чтобы персонажи создавались только один раз
            }
        } else loader.setAttribute('src', './images/error.png')  // показываю знак ошибки 
    })
    xhr.send() // Отсылаю запрос
}

getStars('https://swapi.co/api/people/?page=1', false) // Запускаю функцию


// Переключение страниц внизу
let n = 1
const prev = document.querySelector('#prev'),
next = document.querySelector('#next'),
page = document.querySelector('#page'),

// на страницу вперед
plusPage = () => {
    if(n < 9) {
        n++
        page.innerHTML = ` ${n} / 9 `
        getStars(`https://swapi.co/api/people/?page=${n}`, true)

        // скрываю, чтобы не было видно во время загрузки
        page.classList = 'hide'
        prev.classList = 'hide'
        next.classList = 'hide'
    }
},

// на страницу назад
minusPage = () => {
    if(n > 1) {
        n--
        page.innerHTML = ` ${n} / 9 `
        getStars(`https://swapi.co/api/people/?page=${n}`, true)

        // скрываю, чтобы не было видно во время загрузки
        page.classList = 'hide'
        prev.classList = 'hide'
        next.classList = 'hide'
    }
}

// добавляю функции кнопкам
next.addEventListener('click', plusPage)
prev.addEventListener('click', minusPage)